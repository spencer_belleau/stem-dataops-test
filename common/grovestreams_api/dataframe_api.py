import json
import http.client
import gzip
import pandas as pd
import time
import os
import datetime
import requests
import numpy as np
from pandas.api.types import is_datetime64_any_dtype as is_datetime
from typing import Union
import common.grovestreams_api.definitions as local_definitions
import common.config_tools.manager as config_manager
import common.grovestreams_api.utils as gst_util


local_path = os.path.dirname(os.path.abspath(__file__))
config = config_manager.get_module_config_from_root_path(local_definitions.root_path)


def __convert_dt_to_gst_timestamp(column: pd.Series) -> pd.Series:
    """
    Converts a column of Python datetime objects or string-like datetime representations into UNIX timestamps,
    then multiplies them by 1000 to put them into ms units.

    :param column: The column to covert
    :return: A Pandas Series object
    """
    if is_datetime(column):
        column = column.dt.tz_convert('utc') - pd.to_datetime(datetime.datetime(1970, 1, 1)).tz_localize('utc')
    else:
        column = pd.to_datetime(column, utc=True) - pd.to_datetime(datetime.datetime(1970, 1, 1)).tz_localize('utc')
    return column.dt.total_seconds().astype(np.int64) * 1000


def __chunk_divide_dataframe(df: pd.DataFrame, chunk_size: int) -> list:
    """
    Breaks a dataframe up into a list of dataframes of maximum length chunk_size

    :param df: The dataframe to break up
    :param chunk_size: The maximum chunk size, in rows
    :return: A python list of dataframes, containing the total of the data from df.
    """
    chunk_list = []
    for j in range(0, int(len(df) / chunk_size) + 1):
        chunk_list.append(df[(j * chunk_size):((j + 1) * chunk_size)])
    return chunk_list


def __gzip_json_dict(d: dict, encoding: str) -> bytes:
    """
    Changes a dictionary into a json string, then encodes it with the specified encoding and compresses
    it using gzip.

    :param d: The dictionary to compress
    :param encoding: The encoding to use to turn the string into bytes, ex: 'utf-8'
    :return: The bytes of the compressed json.
    """
    json_encoded = json.dumps(d)
    return gzip.compress(json_encoded.encode(encoding))


def __upload_chunk(data: dict, templateId: str) -> bool:
    """
    Send JSON data to the GroveStreams feed

    :param data: take a json stream data as feed input
    :param templateId: use to create new component if not existing (use template_filter.json to find folder)
    :return: True if operation succeeds
    """
    if len(templateId) == 0:
        url = '/api/feed?autofillGapsWithNulls=false'
    else:
        url = '/api/feed?autofillGapsWithNulls=false&compTmplId=' + templateId

    headers = {"Content-Encoding": "gzip", "Connection": "close",
               "Content-type": "application/json", "Cookie": "api_key=" + config["API_key"]}
    body = __gzip_json_dict(data, 'utf-8')
    return __put_feed(url, body, headers)


def __put_feed(url: str, body: bytes, headers: dict) -> bool:
    """
    Attempts to send a given byte payload to the GroveStreams API

    :param url: The URL path to upload the information to, ex: /api/feed?autofillGapsWithNulls=false
    :param body: The byte information to send to the url.
    :param headers: The required header information for constructing the HTTP request.
    :return: True if upload succeeds, otherwise false.
    """
    conn = None
    try:
        conn = http.client.HTTPConnection('www.grovestreams.com')
        conn.request("PUT", url, body, headers)
        response = conn.getresponse()
        success = (response.reason == 'OK')
        print("Got response code: " + str(response.status))
        if not success:
            return False
        else:
            return True
    except Exception as e:
        print('GST Upload Failed: ' + str(e))
        return False
    finally:
        if conn is not None:
            conn.close()


def upload_dataframe(data_df: pd.DataFrame, templateId: str = '') -> bool:
    """
    Upload a dataframe to GroveStreams

    :param data_df: The dataframe to upload
    :param templateId: GroveStreams template to use (?)
    :return: True if operation succeeds, False otherwise
    """
    if len(data_df) <= 0:
        print("Empty Dataframe - Nothing to upload")
        return False
    data_df.rename(columns={'value': 'data', 'Value': 'data', 'VALUE': 'data',
                            'CompID': 'compId',
                            'StreamID': 'streamId',
                            'dttm': 'starttime', 'BeginDate': 'starttime'}, inplace=True)
    data_df = data_df[~pd.isna(data_df.starttime)]
    data_df['time'] = __convert_dt_to_gst_timestamp(data_df['starttime'])
    chunks = __chunk_divide_dataframe(data_df, config["max_chunk_rows"])
    success = True
    for chunk in chunks:
        chunk_success = __upload_chunk(chunk[['compId', 'streamId', 'data', 'time']].to_dict(orient='records'),
                                       templateId)
        success &= chunk_success
        time.sleep(1)
    if success:
        print("Dataframe Upload Succeeded")
    return success


def get_component_stream(component: str, stream: str, start: datetime.datetime, end: datetime.datetime) \
        -> Union[pd.DataFrame, None]:
    """
    Gets feed data for a given component and stream.

    :param component: The component ID to get
    :param stream: The stream ID to get
    :param start: The time to start fetching data from
    :param end: The time to stop fetching data
    :return: A dataframe containing the requested data, or None if no data was found.
    """
    headers = {"Connection": "close", "Cookie": "api_key=" + config["API_key"]}
    url = '/api/comp/{compId}/stream/{streamId}/feed?sd={start}&ed={end}'
    url = url.format(compId=component, streamId=stream, start=gst_util.datetime_to_gst_time(start),
                     end=gst_util.datetime_to_gst_time(end))
    tries = 0
    while tries < 10:
        try:
            tries += 1
            response = requests.get('http://www.grovestreams.com' + url, headers=headers)
            return pd.DataFrame(response.json())
        except Exception as e:
            print('GST GET Failed: ' + str(e))
            time.sleep(1)
    return None


def get_component_property(component: str, property_name: str) -> Union[pd.DataFrame, None]:
    """
    Gets the value of a given component property.
    Apparently this doesn't actually get custom properties. Those are still streams, for some reason.

    :param component: The component ID to get
    :param property_name: The property ID to get
    :return: A dataframe containing the requested data, or None if no data was found.
    """
    headers = {"Connection": "close", "Cookie": "api_key=" + config["API_key"]}
    url = '/api/comp/{compId}/property?{prop}'
    url = url.format(compId=component, prop=property_name)
    tries = 0
    while tries < 10:
        try:
            tries += 1
            response = requests.get('http://www.grovestreams.com' + url, headers=headers)
            return pd.DataFrame(response.json())
        except Exception as e:
            print('GST GET Failed: ' + str(e))
            time.sleep(1)
    return None


def __try_fetch_interval_data_between_dates(component: str, stream: str, start: datetime.datetime,
                                            end: datetime.datetime) -> pd.DataFrame:
    """
    Tries 10 times to fetch data from a GroveStreams component/stream between two dates.
    Will throw a general exception if data cannot be fetched.
    Will not fetch more than 1000 rows of data.

    :param component: The ID of the component which has the stream.
    :param stream: The stream ID.
    :param start: The date to start pulling data.
    :param end: The date to stop pulling data.
    :return: A dataframe containing all rows which were fetched.
    """
    retries = 0
    while retries < 10:
        frame = get_component_stream(component, stream, start, end)
        if frame is None:
            print("Failed to get GST data, retrying.")
            retries += 1
            continue
        return frame
    raise Exception("GST timed out repeatedly, possible service is down.")


def __fetch_all_interval_data_between_dates(component: str, stream: str, start: datetime.datetime,
                                            end: datetime.datetime, interval: datetime.timedelta) -> pd.DataFrame:
    """
    Tries to fetch data from a GroveStreams component/stream between two dates, using time intervals of
    a given size to do so. Please note, the GroveStreams API can only fetch 1000 rows at a time, so
    the provided interval should result in pulling less than or equal to 1000 rows per API call.

    :param component: The ID of the component which has the stream.
    :param stream: The stream ID.
    :param start: The date to start pulling data.
    :param end: The date to stop pulling data.
    :param interval: The interval which will lead to pulling <= 1000 rows of data
    :return: A dataframe containing all rows which were fetched.
    """
    process_date_start = start
    process_date_end = start + interval
    frames = []
    retries = 0
    while retries < 10:
        frames.append(__try_fetch_interval_data_between_dates(component, stream, process_date_start, process_date_end))
        if process_date_end >= end:
            break
        else:
            process_date_start += interval
            process_date_end += interval
            if process_date_end > end:
                process_date_end = end
    if retries == 10:
        raise Exception("GST timed out repeatedly, possible service is down.")
    return pd.concat(frames)


def get_5min_data_between_dates(component: str, stream: str, start: datetime.datetime, end: datetime.datetime) \
        -> pd.DataFrame:
    """
    Gets all 5 minute interval data for a given stream between two dates.

    :param component: The ID of the component which has the stream.
    :param stream: The stream ID.
    :param start: The date to start pulling data.
    :param end: The date to stop pulling data.
    :return: A dataframe containing all rows which were fetched.
    """
    return __fetch_all_interval_data_between_dates(component, stream, start, end, datetime.timedelta(minutes=5000))


def get_15min_data_between_dates(component: str, stream: str, start: datetime.datetime, end: datetime.datetime) \
        -> pd.DataFrame:
    """
    Gets all 15 minute interval data for a given stream between two dates.

    :param component: The ID of the component which has the stream.
    :param stream: The stream ID.
    :param start: The date to start pulling data.
    :param end: The date to stop pulling data.
    :return: A dataframe containing all rows which were fetched.
    """
    return __fetch_all_interval_data_between_dates(component, stream, start, end, datetime.timedelta(minutes=15000))


def get_hourly_data_between_dates(component: str, stream: str, start: datetime.datetime, end: datetime.datetime) \
        -> pd.DataFrame:
    """
    Gets all hourly interval data for a given stream between two dates.

    :param component: The ID of the component which has the stream.
    :param stream: The stream ID.
    :param start: The date to start pulling data.
    :param end: The date to stop pulling data.
    :return: A dataframe containing all rows which were fetched.
    """
    return __fetch_all_interval_data_between_dates(component, stream, start, end, datetime.timedelta(hours=1000))


def get_daily_data_between_dates(component: str, stream: str, start: datetime.datetime, end: datetime.datetime) \
        -> pd.DataFrame:
    """
    Gets all daily interval data for a given stream between two dates.

    :param component: The ID of the component which has the stream.
    :param stream: The stream ID.
    :param start: The date to start pulling data.
    :param end: The date to stop pulling data.
    :return: A dataframe containing all rows which were fetched.
    """
    return __fetch_all_interval_data_between_dates(component, stream, start, end, datetime.timedelta(days=1000))
