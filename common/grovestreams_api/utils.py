import datetime


def gst_time_to_datetime(gst_time: int) -> datetime.datetime:
    """
    Simple conversion of GroveStreams timestamps to python datetime objects.

    :param gst_time: The GroveStreams timestamp, which is a unix timestamp * 1000
    :return: A datetime object matching the time of the GroveStreams timestamp
    """
    return datetime.datetime.fromtimestamp(gst_time / 1000, tz=datetime.timezone.utc)


def datetime_to_gst_time(dt: datetime.datetime) -> int:
    """
    Simple conversion of Python datetime object to GroveStreams timestamp.

    :param dt: The python datetime object
    :return: An integer which represents the GroveStreams timestamp
    """
    return int(dt.timestamp() * 1000)
