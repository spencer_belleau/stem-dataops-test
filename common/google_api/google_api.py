from google.oauth2 import service_account
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
import numpy as np
import pandas as pd
from apiclient.http import MediaFileUpload,MediaIoBaseDownload,MediaIoBaseUpload
from apiclient import errors
import io
import time
import json
import openpyxl
import os

# import common.config_tools.manager as config_manager
# import common.google_api.definitions as local_definitions

local_path = os.path.dirname(os.path.abspath(__file__))

#%reset -f

creds_file = os.path.join(local_path, 'credentials.json')

'''
Share the folder with: dr-ec2-op@data-ops-329015.iam.gserviceaccount.com
Limitations
Google Drive API
Queries per day: 1,000,000,000
Queries per 100 seconds per user: 10,000
Queries per 100 seconds: 10,000

Google Sheets API
Read requests per minute: 300
Read requests per minute per user: 60
Read requests per day: Unlimited
Write requests per minute: 300
Write requests per minute per user: 60
Write requests per day: Unlimited

drive.push_to_gd_csv(dm_extract, 'FullDatamart.csv', '1iOsCXM_I1NR7hV2cRE31meAVbP9Syt-I')

full_path = "G:\\Shared drives\\Grovestreams\\Import-UtilityAPI\\uAPI Monitoring Setup\\"
parent_id = "Grovestreams"



drive.get_folder_names(parent_id)
drive.get_file_id_from_path(full_path)

drive = DriveAPI()
parent_id = '0AJvvm4YxvN3UUk9PVA'
drive.get_directory(parent_id, folders_only = True)


bill_cyclc_id = "1fFbajWxpz02VWFvbjUVpRZZ-arT3wOqi"
bill_cyclc_dir = drive.get_directory(bill_cyclc_id)
print(bill_cyclc_dir)
for j in range(0, len(bill_cyclc_dir)):
    directory = bill_cyclc_dir[bill_cyclc_dir.index == j]
    print(directory)
    sub_dir = drive.get_directory(directory['folderid'].iloc[0])



file_names = drive.get_file_names(parent_id = "0ANiDzJWXO9__Uk9PVA", folders_only = True, all_drives = True)
'''

class DriveAPI:
    name = 'drive'
    version = 'v3'
    scopes = ['https://www.googleapis.com/auth/drive']

    def __init__(self):
        creds_path = creds_file
        creds = service_account.Credentials.from_service_account_file(creds_path, scopes=self.scopes)
        creds.refresh(Request())
        self.service = build(self.name, self.version, credentials=creds)

    def get_folder_names(self, parent_id=None):
         query = "mimeType = 'application/vnd.google-apps.folder'"
         if parent_id is not None:
             query = query + " and '" + parent_id + "' in parents"
         results = self.service.files().list(
             pageSize=100, fields="nextPageToken, files(id, name)", q=query
             ).execute()
         items = results.get('files', [])
         file_names = dict()
         for item in items:
             file_names[item.get('name')] = item.get('id')
         return file_names

    def __get_files_query_all_drives(self, query):
        page_token = None
        result = []
        while True:
            files = self.service.files().list(pageSize=100,
                                              fields="nextPageToken, files(id, name)",
                                              pageToken=page_token,
                                              q=query,
                                              corpora='allDrives',
                                              supportsAllDrives=True,
                                              includeItemsFromAllDrives=True).execute()
            result.extend(files['files'])
            page_token = files.get('nextPageToken')
            if not page_token:
                break
        return result

    def __get_files_query_shared_drive(self, query, drive_id):
        page_token = None
        result = []
        while True:
            files = self.service.files().list(pageSize=100,
                                              fields="nextPageToken, files(id, name)",
                                              pageToken=page_token,
                                              q=query,
                                              corpora='allDrives',
                                              supportsAllDrives=True,
                                              driveId=drive_id,
                                              includeItemsFromAllDrives=True).execute()
            result.extend(files['files'])
            page_token = files.get('nextPageToken')
            if not page_token:
                break
        return result

    def __get_files_query_user(self, query):
        page_token = None
        result = []
        while True:
            files = self.service.files().list(pageSize=100,
                                              fields="nextPageToken, files(id, name)",
                                              pageToken=page_token,
                                              q=query,
                                              corpora='user',
                                              supportsAllDrives=True,
                                              includeItemsFromAllDrives=True).execute()
            result.extend(files['files'])
            page_token = files.get('nextPageToken')
            if not page_token:
                break
        return result

    def get_directory(self, parent_id, folders_only = True):
        try:
            if folders_only:
                query = "mimeType = 'application/vnd.google-apps.folder' and '" + parent_id + "' in parents"
            else:
                query = "'" + parent_id + "' in parents"
            result = self.__get_files_query_all_drives(query)
            results = pd.DataFrame(result).rename(columns = {'id':'folderid','name':'folder'})
            time.sleep(3)
        except:
                time.sleep(6)
                results = self.get_directory(self, parent_id, folders_only)
        return results

    def get_file_names(self, starts_with=None, contains=None, case_sensitive=True, 
                       folders_only=False, parent_id=None, all_drives=True, shared_drive_id=None):
        query_ls = list()
        if folders_only:
            query_ls.append("mimeType = 'application/vnd.google-apps.folder'")
        if parent_id is not None:
            query_ls.append("'" + parent_id + "' in parents")
        
        if len(query_ls) == 0:
            query = None
        else:
            query = " and ".join(query_ls)
        if all_drives:
            items = self.__get_files_query_all_drives(query)
        elif shared_drive_id is not None:
            items = self.__get_files_query_shared_drive(query, shared_drive_id)
        else:
            items = self.__get_files_query_user(query)
        matching_list = list()

        for item in items:
            if starts_with is not None:
                if case_sensitive:
                    if item.get('name').startswith(starts_with):
                        matching_list.append(item)
                else:
                    if item.get('name').lower().startswith(starts_with):
                        matching_list.append(item)
            elif contains is not None:
                if case_sensitive:
                    if contains in item.get('name'):
                        matching_list.append(item)
                else:
                    if contains.lower() in item.get('name').lower():
                        matching_list.append(item)
            else:
                matching_list.append(item)
        
        file_names = pd.DataFrame.from_records(matching_list)
        if len(file_names) > 0:
            file_names.rename(columns={'id': 'fileid', 'name': 'filename'}, inplace=True)
            return file_names
        else:
            return pd.DataFrame(columns=['filename', 'fileid'])

    def get_last_file_id(self, path_sections, parent_id, all_drives=False, shared_drive_id=None):
        files_found = self.get_file_names(
            equals=path_sections[0], parent_id=parent_id, 
            all_drives=all_drives, shared_drive_id=shared_drive_id)
        if len(files_found) > 1:
            raise ValueError('Found multiple files/folders with name '+str(path_sections[0])+' under '+parent_id)
        path_sections = path_sections[1:]
        parent_id = list(files_found.keys())[0]
        if len(path_sections) > 0:
            return self.get_last_file_id(
                path_sections, parent_id, all_drives=all_drives, 
                shared_drive_id=shared_drive_id)
        else:
            return files_found

    def get_file_id_from_path(self, full_path, all_drives=False, shared_drive_id=None):
        path_sections = full_path.split('G:\\Shared drives\\')[1].split('\\')
        first_file_id = self.get_last_file_id(
            path_sections, all_drives=all_drives, shared_drive_id=shared_drive_id)
        first_file_name = list(first_file_id.values())[0]
        first_file_id = list(first_file_id.keys())[0]
        first_file_index = path_sections.index(first_file_name)
        path_sections = path_sections[ (first_file_index+1): ]
        last_file_id = self.get_last_file_id(
            path_sections, first_file_id, all_drives=all_drives, 
            shared_drive_id=shared_drive_id)
        return last_file_id

    def create_gsheet(self, file_name, folder_id):
        file_metadata = {
            'name': file_name,
            'mimeType': 'application/vnd.google-apps.spreadsheet',
            'parents': [folder_id],
            'uploadType': 'resumable'
            }
        file = self.service.files().create(body=file_metadata,
                                           fields='id').execute()
        return file.get('id')

    def create_exsheet(self, file_name,source,folder_id):
        file_metadata = {
            'name': file_name,
            'mimeType': 'application/vnd.ms-excel',
            'parents': [folder_id],
            'uploadType': 'resumable'
            }
        media = MediaFileUpload(source, mimetype='application/vnd.ms-excel')
        #application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        #application/vnd.ms-excel
        #application/vnd.google-apps.spreadsheet (top one)
        file = self.service.files().create(body=file_metadata,media_body=media,
                                           fields='id').execute()
        return file.get('id')

    def print_file_metadata(self,file_id):
      try:
        file = self.service.files().get(fileId=file_id).execute()
    
        print('Title: %s' % file['title'])
        print('MIME type: %s' % file['mimeType'])
      except(errors.HttpError, errors):
        print('An error occurred: %s' % errors)

    def print_file_content(self, file_id):
      file=''
      try:
        file=self.service.files().get_media(fileId=file_id).execute()
        toread = io.BytesIO()
        toread.write(file)  # pass your `decrypted` string as the argument here
        toread.seek(0)  # reset the pointer

        df = pd.read_excel(toread)
      except(errors.HttpError, errors):
        print('An error occurred: %s' % errors)
        
        return df

    def download_csv_to_df(self, file_id, delimiter = None, error_bad_lines = True):
      request = self.service.files().get_media(fileId=file_id).execute()
      toread = io.BytesIO()
      toread.write(request)  # pass your `decrypted` string as the argument here
      toread.seek(0)  # reset the pointer

      df = pd.read_csv(toread, delimiter = delimiter, error_bad_lines = error_bad_lines)
      return df

    def download_xlsx_to_df(self, file_id, sheet_name, skiprows = 0, na_filter = True):
      request = self.service.files().get_media(fileId=file_id).execute()
      toread = io.BytesIO()
      toread.write(request)
      toread.seek(0)

      df = pd.read_excel(toread, sheet_name=sheet_name, skiprows = skiprows, na_filter = na_filter)
      return df

    #file_id = "1b-qLa3C-K7beCM84Ujsw4IK5jphizKE6"
    def get_flat_file(self, file_id):
        request = self.service.files().get_media(fileId=file_id)
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print("Download %d%%." % int(status.progress() * 100))
        fh.seek(0)
        data = fh.read()
        data = json.loads(data)
        return data

    def download_file_2(self,file_id,file_location):
        #works for xlsx and csv. Must name file extension in file_location
        #file_id = "1b-qLa3C-K7beCM84Ujsw4IK5jphizKE6"
        request = self.service.files().get_media(fileId=file_id)
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print("Download %d%%." % int(status.progress() * 100))
        fh.seek(0)
        fh.read()
        with open(file_location, 'wb') as f:
            f.write(fh.read())
            f.close()
        return

    def download_xlsx_to_df2(self, file_id,sheet_name):
      request = self.service.files().get_media(fileId=file_id).execute()
      toread = io.BytesIO()
      toread.write(request)  # pass your `decrypted` string as the argument here
      toread.seek(0)  # reset the pointer

      wb = openpyxl.load_workbook(toread)
      ws = wb[sheet_name]

      df = pd.DataFrame(ws.values)
      return df

    def get_metadata(self, file_id, fields='*'):
        metadata = self.service.files().get(fileId=file_id, fields=fields, supportsAllDrives=True).execute()
        metadata = pd.DataFrame(metadata.items()).rename(columns = {0:'attribute',1:'value'})
        return metadata

    def write_xlsx(self, xlsxinput, file_name, folder_id):
        #input is xlsx file in bytes format, 
        toread=io.BytesIO(xlsxinput)
        toread.seek(0)  # reset the pointer
        media = MediaIoBaseUpload(toread,mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        file_metadata = {'name': file_name,'parents':[folder_id]}
        request=self.service.files().create(body=file_metadata, media_body=media, supportsAllDrives=True).execute()
        print('File ID: %s' % request.get('id'))
        return request.get('id')

    def update_xlsx(self, xlsxinput, file_name, folder_id):
        #input is xlsx file in bytes format, 
        toread=io.BytesIO(xlsxinput)
        toread.seek(0)  # reset the pointer
        media = MediaIoBaseUpload(toread,mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        file_metadata = {'name': file_name, 'fileId': file_name, 'parents':[folder_id]}
        request=self.service.files().update(body=file_metadata, media_body=media, supportsAllDrives=True).execute()
        print('File ID: %s' % request.get('id'))
        return request.get('id')

    def move_f(self, file_id, new_parent, current_parent = None, copy = False):
        if current_parent == None:
            file = self.service.files().get(fileId=file_id, fields='parents',supportsAllDrives = True).execute()
            current_parent = ",".join(file.get('parents'))
        if copy == False:
            file = self.service.files().update(fileId=file_id, addParents=new_parent,
                                            removeParents=current_parent, fields='id, parents',
                                            supportsAllDrives = True).execute()
        else:
            file_metadata = {'fileId': file_id, 'parents':[new_parent]}
            file = self.service.files().copy(fileId=file_id, body=file_metadata,
                                             fields='id, parents', supportsAllDrives = True).execute()

    def write_csv(self, dfinput, file_name, folder_id, index_out = False):
        #folder_id = "1WG7Yas6CnbLuCFQZaMWKlH8jL2kTr5-I"
        #file_name = "uid_flags2021-02-16.csv"
        #index_out = False
        #dfinput = uid_flags
        csvinput = bytes(dfinput.to_csv(index = index_out),encoding='utf-8')
        toread = io.BytesIO(csvinput)
        toread.seek(0)  # reset the pointer
        media = MediaIoBaseUpload(toread, mimetype='text/csv')
        
        file_names = self.get_file_names(parent_id = folder_id, all_drives = True)
        existing_file = file_names[file_names.filename == file_name]
        
        if len(existing_file) > 0:
            existing_file_id = existing_file.fileid.iloc[0]
            file_metadata = {'name': file_name}
            request = self.service.files().update(body=file_metadata, 
                                                  fileId = existing_file_id, 
                                                  media_body=media, 
                                                  supportsAllDrives=True).execute()
        else:
            file_metadata = {'name': file_name,'parents':[folder_id]}
            request = self.service.files().create(body=file_metadata, 
                                                  media_body=media, 
                                                  supportsAllDrives=True).execute()
        print('File ID: %s' % request.get('id'))
        return request.get('id')

    def write_graph(self, plt, file_name, folder_id):
        #folder_id = "1WG7Yas6CnbLuCFQZaMWKlH8jL2kTr5-I"
        #file_name = "ghg.png"
        toread = io.BytesIO()
        plt.savefig(toread, format='png')
        plt.close
        toread.seek(0)
        media = MediaIoBaseUpload(toread, mimetype='image/jpeg')
        file_metadata = {'name': file_name,'parents':[folder_id]}
        
        file_names = self.get_file_names(parent_id = folder_id, all_drives = True)
        existing_file = file_names[file_names.filename == file_name]
        
        if len(existing_file) > 0:
            existing_file_id = existing_file.fileid.iloc[0]
            file_metadata = {'name': file_name}
            request = self.service.files().update(body=file_metadata, 
                                                  fileId = existing_file_id, 
                                                  media_body=media, 
                                                  supportsAllDrives=True).execute()
        else:
            request = self.service.files().create(body=file_metadata, 
                                                  media_body=media, 
                                                  supportsAllDrives=True).execute()
        print('File ID: %s' % request.get('id'))
        return request.get('id')

    def write_file(self, file_input, file_name, folder_id):
        toread=io.BytesIO(file_input)
        toread.seek(0)  # reset the pointer
        if 'csv' in file_name: media = MediaIoBaseUpload(toread,mimetype='application/csv')
        elif 'json' in file_name: media = MediaIoBaseUpload(toread,mimetype='application/json')
        elif 'pdf' in file_name: media = MediaIoBaseUpload(toread,mimetype='application/pdf')
        elif 'xml' in file_name: media = MediaIoBaseUpload(toread,mimetype='application/xml')
        elif 'xlsx' in file_name: media = MediaIoBaseUpload(toread,mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        file_names = self.get_file_names(parent_id = folder_id, all_drives = True)
        existing_file = file_names[file_names.filename == file_name]
        
        if len(existing_file) > 0:
            existing_file_id = existing_file.fileid.iloc[0]
            file_metadata = {'name': file_name}
            request = self.service.files().update(body=file_metadata, 
                                                  fileId = existing_file_id, 
                                                  media_body=media, 
                                                  supportsAllDrives=True).execute()
        else:
            file_metadata = {'name': file_name,'parents':[folder_id]}
            request = self.service.files().create(body=file_metadata, 
                                                  media_body=media, 
                                                  supportsAllDrives=True).execute()
        print('File ID: %s' % request.get('id'))
        return request.get('id')

    def update_csv(self, dfinput, file_name, folder_id, index_out = False):
        csvinput=bytes(dfinput.to_csv(index = index_out),encoding='utf-8')
        toread=io.BytesIO(csvinput)
        toread.seek(0)  # reset the pointer
        media = MediaIoBaseUpload(toread,mimetype='text/csv')
        file_metadata = {'name': file_name, 'fileId': file_name, 'parents':[folder_id]}
        request=self.service.files().update(body=file_metadata, media_body=media, supportsAllDrives=True).execute()
        print('File ID: %s' % request.get('id'))
        return request.get('id')

    def create_folder(self, folder_name, parent_folder_id):
        file_metadata = {'name': folder_name,
                         'parents':[parent_folder_id], 
                         'mimeType' :'application/vnd.google-apps.folder'}
        request = self.service.files().create(body=file_metadata, 
                                             supportsAllDrives=True, 
                                             fields='id').execute()
        return request.get('id')
    #Permissions failing on this. I'll use a "trash" functionality for now instead
    def delete_f(self, file_name):
        request = self.service.files().delete(fileId= file_name, supportsAllDrives=True).execute()
        return request


class SheetsAPI:
    name = 'sheets'
    version = 'v4'
    scopes = ['https://www.googleapis.com/auth/spreadsheets']
    
    def __init__(self):
        creds_path = creds_file
        creds = service_account.Credentials.from_service_account_file(creds_path, scopes=self.scopes)
        creds.refresh(Request())
        self.service = build(self.name, self.version, credentials=creds)

    def download_gsheet(self, file_id, sheet_name, sheet_range):
        name_range = "'" + sheet_name + "'!" + sheet_range
        gsheet = self.service.spreadsheets().values().get(spreadsheetId=file_id, range=name_range).execute()
        return gsheet

    def gsheet2df(self, gsheet):
        header = gsheet.get('values', [])[0]   # Assumes first line is header!
        values = gsheet.get('values', [])[1:]  # Everything else is data.
        data = np.empty((len(values),len(header)))
        data[:] = np.nan
        df = pd.DataFrame(data, columns=header)
        for row_id in range(len(values)):
            row_length = len(values[row_id])
            count = 0
            for col_id, col_name in enumerate(header):
                if count < row_length:
                    df.iloc[row_id, col_id] = values[row_id][col_id]
                count += 1
        return df

    def df2gsheet(self, file_id, df, clear_first=True, max_range='A:AZ'):
        sheets = self.service.spreadsheets()
        if clear_first:
            batchclear_body = {
                'ranges': [max_range]
            }
            request = sheets.values().batchClear(spreadsheetId=file_id, body=batchclear_body)
            response = request.execute()
        batchupdate_body = {
            "valueInputOption": "RAW",
            "data": [
                {
                    "range": "Sheet1!A1",
                    "majorDimension": "ROWS",
                    "values": [df.columns.tolist()]
                    },
                {
                    "range": "Sheet1!A2",
                    "majorDimension": "ROWS",
                    "values": df.values.tolist()
                    }
                ]
            }
        request = sheets.values().batchUpdate(spreadsheetId=file_id, body=batchupdate_body)
        response = request.execute()
        return response
    
    def df2gsheet_range(self, file_id, df, clear_first=True, max_range='A:AZ',HeaderCorner="Sheet1!A1",DataCorner="Sheet1!A2"):
        sheets = self.service.spreadsheets()
        if clear_first:
            batchclear_body = {
                'ranges': [max_range]
            }
            request = sheets.values().batchClear(spreadsheetId=file_id, body=batchclear_body)
            response = request.execute()
        batchupdate_body = {
            "valueInputOption": "RAW",
            "data": [
                {
                    "range": HeaderCorner,
                    "majorDimension": "ROWS",
                    "values": [df.columns.tolist()]
                    },
                {
                    "range": DataCorner,
                    "majorDimension": "ROWS",
                    "values": df.values.tolist()
                    }
                ]
            }
        request = sheets.values().batchUpdate(spreadsheetId=file_id, body=batchupdate_body)
        response = request.execute()
        return response


if __name__ == "__main__":
    pass