import os
import requests
import pandas as pd
import common.config_tools.manager as config_manager
import common.salesforce_api.definitions as local_definitions

local_path = os.path.dirname(os.path.abspath(__file__))
config = config_manager.get_module_config_from_root_path(local_definitions.root_path)


class SFSelectQueryError(Exception):
    """
    Raised when the REST API cannot process a SELECT query sent to it.
    """
    pass


def get_access_info() -> dict:
    """
    Gets the salesforce REST access information from the salesforce login page.
    When successful, wll always contain (among other things): access_token, instance_url.

    :return: A dictionary representing the json result of the request.
    """
    payload = {
            'grant_type': 'password',
            'client_id': config['client_id'],
            'client_secret': config['client_secret'],
            'username': config['username'],
            'password': config['password'] + config['security_token']
             }
    r = requests.post("https://login.salesforce.com/services/oauth2/token",
                      headers={"Content-Type": "application/x-www-form-urlencoded"},
                      data=payload)
    body = r.json()
    return body


def construct_query(requested_variables: list, table: str, order_by: str = None, where: str = None):
    """
    Constructs a list of query tokens from a simplified set of arguments.

    :param requested_variables: The object variables to query, as a list of strings
    :param table: The object to query, as a string
    :param order_by: Which of the variables to use for ordering. Note: Must be a string in requested_variables. Optional
    :param where: A SQL-like where clause, ex "column1 >= 200". Optional.
    :return: A list of strings representing the tokens of the query for use in get_full_query_results
    """
    query_tokens = ["SELECT", ','.join(requested_variables), "FROM", table]
    if where is not None:
        query_tokens += ["WHERE", *(where.split(' '))]
    if order_by is not None:
        query_tokens += ["ORDER", "BY", order_by]
    query_tokens += ["LIMIT", "20000"]
    return query_tokens


def get_full_query_results(access_token: str, instance_url: str, query_tokens: list) -> list:
    """
    Gets all results for a given query string using the salesforce REST API.

    :param access_token: The access token for the SFDB instance
    :param instance_url: The URL for the SFDB instance
    :param query_tokens: The query to process, as a list of string tokens.
    :return: A list of dictionaries for each group of 2000 results.
    """
    data = []
    url = instance_url + "/services/data/v52.0/query/?q=" + '+'.join(query_tokens)
    all_fetched = False
    while not all_fetched:
        results = requests.get(url, headers={"Authorization": "Bearer " + access_token}).json()
        if type(results) is list:
            if 'errorCode' in results[0].keys():
                raise SFSelectQueryError(f"Failed to fetch results. Error Code {results[0]['errorCode']},"
                                         f" Message: {results[0]['message']}")
        data.append(results)
        if 'nextRecordsUrl' in results.keys():
            url = instance_url + results['nextRecordsUrl']
        else:
            all_fetched = True
    return data


def get_records_df(access_token: str, instance_url: str, requested_variables: list, table: str, order_by: str = None,
                   where: str = None):
    """
    Creates and executes a SOQL query on a salesforce instance and returns the resulting rows as a dataframe.

    :param access_token: The access token for the SFDB instance
    :param instance_url: The URL for the SFDB instance
    :param requested_variables: The object variables to query, as a list of strings
    :param table: The object to query, as a string
    :param order_by: Which of the variables to use for ordering. Note: Must be a string in requested_variables. Optional
    :param where: A SQL-like where clause, ex "column1 >= 200". Optional.
    :return: A dataframe containing all columns and rows returned by the rest API
    """
    query = construct_query(requested_variables, table, order_by, where)
    raw_data = get_full_query_results(access_token, instance_url, query)
    return_data = []
    for part in raw_data:
        return_data.extend(part['records'])
    return pd.DataFrame(return_data)


def get_records_df_query_default_access(access: dict, query_string: str) -> pd.DataFrame:
    """
    Executes a SOQL query on a salesforce instance and returns the resulting rows as a dataframe.
    Assumes the structure of the access object provided to match that of get_access_info()

    :param access: The access dictionary returned by get_access_info()
    :param query_string: The query, as a normal SQL-like string.
    :return: A dataframe containing all columns and rows returned by the rest API
    """
    return get_records_df_query(access['access_token'], access['instance_url'], query_string)


def get_records_df_query(access_token: str, instance_url: str, query_string: str) -> pd.DataFrame:
    """
    Executes a SOQL query on a salesforce instance and returns the resulting rows as a dataframe.

    :param access_token: The access token for the SFDB instance
    :param instance_url: The URL for the SFDB instance
    :param query_string: The query, as a normal SQL-like string.
    :return: A dataframe containing all columns and rows returned by the rest API
    """
    raw_data = get_full_query_results(access_token, instance_url, query_string.split(' '))
    return_data = []
    for part in raw_data:
        return_data.extend(part['records'])
    return pd.DataFrame(return_data)
