import requests
import datetime
import os
from requests.auth import HTTPBasicAuth
from typing import Union
import common.config_tools.manager as config_manager
import common.watttime_api.definitions as local_definitions

# Units are pounds per megawatt hour (lb/MWh), Will need conversion
# All times should be UTC
# base_url is also included in credentials, but not fetched because any change to it should also trigger a code change
# This API is not fully complete


base_url = 'https://api2.watttime.org/v2'
local_path = os.path.dirname(os.path.abspath(__file__))
config = config_manager.get_module_config_from_root_path(local_definitions.root_path)


def new_user(username: str, password: str, email: str, org: str) -> dict:
    """
    Create a new user

    :param username: The new user's username
    :param password: The new user's password
    :param email: The new user's email
    :param org: The new user's organization
    :return: A JSON object representing the API's response
    """
    register_url = base_url + '/register'
    params = {'username': username, 'password': password, 'email': email, 'org': org}
    rsp = requests.post(register_url, json=params)
    return rsp.json()


def login_with_credentials(username: str, password: str) -> Union[str, None]:
    """
    Log in using the specified credentials.
    Login tokens expire 30 minutes after initial creation.

    :return: A string token for the session, or None if login fails
    """
    login_url = base_url + '/login'
    rsp = requests.get(login_url, auth=HTTPBasicAuth(username, password))
    if rsp.status_code == 200:
        return rsp.json()['token']
    else:
        return None


def login() -> str:
    """
    Log in using the default credentials in config.json
    Login tokens expire 30 minutes after initial creation.

    :return: A string token for the session, or None if login fails
    """
    return login_with_credentials(config["username"], config["password"])


def password_reset(username: str) -> dict:
    """
    Send a password reset email to the given user's email

    :param username: The user to send the password reset email to
    :return: A JSON object representing the API's response
    """
    password_url = base_url + '/password/?username=' + username
    rsp = requests.get(password_url)
    return rsp.json()


def determine_grid_region_coords(token, latitude, longitude):
    """
    Determines which Balancing Authority applies to a given set of latitude and longitude co-ordinates

    :param token: Session token from a successful login() call
    :param latitude: The latitude of to use for determining balancing authority
    :param longitude: The longitude of to use for determining balancing authority
    :return: A JSON object representing the API's response
    """
    region_url = base_url + '/ba-from-loc'
    headers = {'Authorization': 'Bearer {}'.format(token)}
    params = {'latitude': str(latitude), 'longitude': str(longitude)}
    rsp = requests.get(region_url, headers=headers, params=params)
    return rsp.json()


def real_time_emissions_index(token, balancing_authority, style="all"):
    index_url = 'https://api2.watttime.org/index'
    headers = {'Authorization': 'Bearer {}'.format(token)}
    params = {'ba': balancing_authority, 'style': style}
    rsp = requests.get(index_url, headers=headers, params=params)
    return rsp.json()


def real_time_emissions_index_coords(token, latitude, longitude, style="all"):
    index_url = 'https://api2.watttime.org/index'
    headers = {'Authorization': 'Bearer {}'.format(token)}
    params = {'latitude': str(latitude), 'longitude': str(longitude), 'style': style}
    rsp = requests.get(index_url, headers=headers, params=params)
    return rsp.json()


def grid_emissions_data(token: str, balancing_authority: str, start_time: datetime.datetime = None,
                        end_time: datetime.datetime = None, style: str = "all") -> dict:
    """
    Get emissions for a given balancing authority over time.
    If start_time or end_time is provided as a string, format must be yyyy-mm-ddThh:MM:ss-zone

    :param token: Session token from a successful login() call
    :param balancing_authority: The balancing authority, as a string
    :param start_time: Time to start gathering data from, optional, must include end_time if included
    :param end_time: Time to stop gathering data, optional, must include start_time if included
    :param style: What type of data to provide. Can be "all" or "moer", optional and defaults to "all"
    :return: A JSON object representing the API's response
    """
    data_url = base_url + '/data'
    headers = {'Authorization': 'Bearer {}'.format(token)}
    params = {'ba': balancing_authority, 'style': style}
    if (start_time is not None) and (end_time is not None):
        params['starttime'] = start_time
        params['endtime'] = end_time
    rsp = requests.get(data_url, headers=headers, params=params)
    return rsp.json()


def grid_emissions_data_coords(token, latitude, longitude, start_time, end_time, style="all"):
    """
    Get marginal emissions for a given balancing authority over time.
    If start_time or end_time is provided as a string, format must be yyyy-mm-ddThh:MM:ss-zone

    :param token: Session token from a successful login() call
    :param latitude: The latitude of to use for determining balancing authority
    :param longitude: The longitude of to use for determining balancing authority
    :param start_time: Time to start gathering data from, optional, must include end_time if included
    :param end_time: Time to stop gathering data, optional, must include start_time if included
    :param style: What type of data to provide. Can be "all" or "moer", optional and defaults to "all"
    :return: A JSON object representing the API's response
    """
    data_url = base_url + '/data'
    headers = {'Authorization': 'Bearer {}'.format(token)}
    params = {'latitude': str(latitude), 'longitude': str(longitude), 'starttime': start_time, 'endtime': end_time,
              'style': style}
    rsp = requests.get(data_url, headers=headers, params=params)
    return rsp.json()


def historical_emissions(token: str, ba: str) -> bytes:
    """
    Gets the historical data from a given balancing authority. Returns data as a zip of monthly csv files.

    :param token: Session token from a successful login() call
    :param ba: The balancing authority, as a string
    :return: The bytes of a zip file containing the requested historical data
    """
    historical_url = base_url + '/historical'
    headers = {'Authorization': 'Bearer {}'.format(token)}
    params = {'ba': ba}
    rsp = requests.get(historical_url, headers=headers, params=params)
    return rsp.content


def emissions_forecast(token: str, ba: str, start_time: datetime.datetime = None,
                       end_time: datetime.datetime = None) -> dict:
    """
    Get emissions forecast data for specified timeframe and balancing authority.
    Datetime formats are yyyy-mm-ddThh:MM:ss-zone

    :param token: Session token from a successful login() call
    :param ba: The balancing authority, as a string
    :param start_time: Time to start forecasting from, optional, must include end_time if included
    :param end_time: Time to stop forecasting, optional, must include start_time if included
    :return: A JSON object representing the API's response
    """
    forecast_url = base_url + '/forecast'
    headers = {'Authorization': 'Bearer {}'.format(token)}
    params = {'ba': ba}
    if start_time is not None:
        params['starttime'] = start_time
    if end_time is not None:
        params['endtime'] = end_time
    rsp = requests.get(forecast_url, headers=headers, params=params)
    return rsp.json()
