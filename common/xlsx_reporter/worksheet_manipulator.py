import openpyxl
import numbers
import common.xlsx_reporter.definitions as local_definitions


def set_cell_str(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int, value) -> None:
    """
    Sets a cell in the given sheet to a string representing the provided value.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the cell on
    :param col: The column to set the cell on
    :param value: Any argument which can be converted to a string.
    """
    sheet.cell(row, col).value = str(value)


def set_cell_float(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int, value) -> None:
    """
    Sets a cell in the given sheet to a string representing the floating point interpretation of the provided value.
    Formats the number to have only two decimal places, for readability.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the cell on
    :param col: The column to set the cell on
    :param value: Any argument which can be converted to a float.
    """
    sheet.cell(row, col).value = "{:.2f}".format(float(value))


def set_cell_int(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int, value) -> None:
    """
    Sets a cell in the given sheet to a string representing the integer interpretation of the provided value.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the cell on
    :param col: The column to set the cell on
    :param value: Any argument which can be converted to an integer.
    """
    sheet.cell(row, col).value = str(int(value))


# noinspection PyTypeChecker
def format_two_decimal_places(value) -> str:
    """
    Formats a cell value with 2 decimal places.

    :param value: The value to format.
    :return: The value with two decimal places, or the value if not applicable.
    """
    if isinstance(value, numbers.Number):
        return __remove_negative_zero_str("{:.2f}".format(float(value)))
    else:
        return str(value)


# noinspection PyTypeChecker
def format_currency(value) -> str:
    """
    Formats a cell value as a currency number with 2 decimal places.

    :param value: The value to format.
    :return: The value as a currency number, or the value if not applicable.
    """
    if isinstance(value, numbers.Number):
        return "$" + format_two_decimal_places(value)
    else:
        return str(value)


# noinspection PyTypeChecker
def format_percent(value) -> str:
    """
    Formats a cell value as a percentage with 2 decimal places.

    :param value: The value to format.
    :return: The value as a percent, or the value if not applicable.
    """
    if isinstance(value, numbers.Number):
        return format_two_decimal_places(value) + "%"
    else:
        return str(value)


# noinspection PyTypeChecker
def format_percent_100(value) -> str:
    """
    Formats a cell value as a percentage with 2 decimal places. Multiplies the value by 100 before formatting.

    :param value: The value to format.
    :return: The value as a percent, or the value if not applicable.
    """
    if isinstance(value, numbers.Number):
        return format_two_decimal_places(value*100.0) + "%"
    else:
        return str(value)


def __remove_negative_zero_str(float_str: str) -> str:
    """
    Quick filter to improve readability of floats outputted by the format_* functions.
    -0 is a valid float number, but for all normal situations it is the same as 0.

    :param float_str: A string representation of a float.
    :return: The corrected string representation of the float.
    """
    if float_str == '-0.00':
        return '0.00'
    return float_str


def apply_header_formatting(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's formatting to the default for header cells in Stem reports
    Note: This function gives an erroneous warning about the cell attribute being readonly. This is false.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the formatting on
    :param col: The column to set the formatting on
    """
    outline_cell(sheet, row, col)
    shade_cell_stem_orange(sheet, row, col)
    font_cell_header(sheet, row, col)


def apply_default_formatting(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's formatting to the default for data cells in Stem reports
    Note: This function gives an erroneous warning about the cell attribute being readonly.
    This is false, and caused by an error in the openpyxl version being used.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the formatting on
    :param col: The column to set the formatting on
    """
    outline_cell(sheet, row, col)
    font_cell_default(sheet, row, col)


# noinspection PyDunderSlots,PyUnresolvedReferences
def font_cell_default(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's font to the default for Stem reports as defined in definitions.py
    Note: This function gives an erroneous warning about the cell attribute being readonly.
    This is false, and caused by an error in the openpyxl version being used.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the font on
    :param col: The column to set the font on
    """
    sheet.cell(row, col).font = local_definitions.default_font


# noinspection PyDunderSlots,PyUnresolvedReferences
def font_cell_header(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's font to the header font for Stem reports as defined in definitions.py
    Note: This function gives an erroneous warning about the cell attribute being readonly.
    This is false, and caused by an error in the openpyxl version being used.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the font on
    :param col: The column to set the font on
    """
    sheet.cell(row, col).font = local_definitions.column_header_font


# noinspection PyDunderSlots,PyUnresolvedReferences
def shade_cell_stem_orange(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's color to the Stem orange color used in reports defined in definitions.py
    Note: This function gives an erroneous warning about the cell attribute being readonly.
    This is false, and caused by an error in the openpyxl version being used.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the color on
    :param col: The column to set the color on
    """
    sheet.cell(row, col).fill = local_definitions.stem_orange_fill


# noinspection PyDunderSlots,PyUnresolvedReferences
def outline_cell(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's outline to the default outline for data cells used in reports defined in definitions.py
    Note: This function gives an erroneous warning about the cell attribute being readonly.
    This is false, and caused by an error in the openpyxl version being used.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the outline on
    :param col: The column to set the outline on
    """
    sheet.cell(row, col).style = local_definitions.style_bolded


# noinspection PyDunderSlots,PyUnresolvedReferences
def set_cell_a1(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's font settings to match the normal A1 cell font for reports.
    Note: This function gives an erroneous warning about the cell attribute being readonly.
    This is false, and caused by an error in the openpyxl version being used.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the outline on
    :param col: The column to set the outline on
    """
    sheet.cell(row, col).font = local_definitions.A1_cell_font


# noinspection PyDunderSlots,PyUnresolvedReferences
def set_cell_bold_text(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    """
    Sets the cell's font settings to have bolded text.
    Note: This function gives an erroneous warning about the cell attribute being readonly.
    This is false, and caused by an error in the openpyxl version being used.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the outline on
    :param col: The column to set the outline on
    """
    sheet.cell(row, col).font = local_definitions.bolded_cell_font


def estimate_column_width_from_cell(sheet: openpyxl.workbook.workbook.Worksheet, row: int, col: int):
    pass
    # sheet.column_dimensions[openpyxl.utils.get_column_letter(col)].width = len(str(sheet.cell(row, col).value))