import os
import openpyxl.styles

root_path = os.path.dirname(os.path.abspath(__file__))

side_thin = openpyxl.styles.Side(style="thin", color="000000")
border_thin = openpyxl.styles.Border(top=side_thin, left=side_thin, right=side_thin, bottom=side_thin)
style_bolded = openpyxl.styles.NamedStyle("Outlined", border=border_thin)

stem_orange = 'FCD5B4'
stem_orange_fill = openpyxl.styles.PatternFill(start_color=stem_orange, end_color=stem_orange, fill_type='solid')

default_font = openpyxl.styles.Font(name='Calibri', size=11)
column_header_font = openpyxl.styles.Font(name='Calibri', size=11, bold=True)
bolded_cell_font = openpyxl.styles.Font(name='Calibri', size=12, bold=True)
A1_cell_font = openpyxl.styles.Font(name='Calibri', size=16, bold=True, color='ff6600')
