import openpyxl
import pandas as pd
import common.xlsx_reporter.worksheet_manipulator as ws_funcs
from collections.abc import Iterable


def write_dataframe_column_headers(df: pd.DataFrame, sheet: openpyxl.workbook.workbook.Worksheet,
                                   col_start: int, row_start: int):
    """
    Writes a dataframe to a given position in a worksheet, including the column names as the first row.

    :param df: The dataframe to write.
    :param sheet: The worksheet to write to
    :param col_start: The column to start writing on (the furthest left column)
    :param row_start: The row to start writing on (the highest row)
    :return: The row number of the first blank row below the dataframe which has been outputted.
    """
    __write_header(df.columns, sheet, col_start, row_start)
    return write_dataframe(df, sheet, col_start, row_start + 1)


def __write_header(names: Iterable, sheet: openpyxl.workbook.workbook.Worksheet, col_start: int, row: int):
    col = col_start
    for element in names:
        ws_funcs.set_cell_str(sheet, row, col, element)
        ws_funcs.apply_header_formatting(sheet, row, col)
        ws_funcs.estimate_column_width_from_cell(sheet, row, col)
        col += 1


def write_dataframe(df: pd.DataFrame, sheet: openpyxl.workbook.workbook.Worksheet, col_start: int, row_start: int):
    """
    Writes a dataframe to a given position in a worksheet.

    :param df: The dataframe to write.
    :param sheet: The worksheet to write to
    :param col_start: The column to start writing on (the furthest left column)
    :param row_start: The row to start writing on (the highest row)
    :return: The row number of the first blank row below the dataframe which has been outputted.
    """

    col = col_start
    row = row_start
    for record in df.itertuples(index=False):
        for val in record:
            ws_funcs.set_cell_str(sheet, row, col, val)
            ws_funcs.apply_default_formatting(sheet, row, col)
            col += 1
        col = col_start
        row += 1
    return row
