import os
import json

local_path = os.path.dirname(os.path.abspath(__file__))


def __get_module_path_from_name(module_import_name: str) -> str:
    """
    Gets the path to a module using its __name__ attribute.
    This function assumes the file structure being used in this project, which is to say it assumes
    that the module it is located inside is in a sub-folder of the project root, and that
    anything it is locating is both a module with an __init__.py and located within a subfolder
    of the project root.

    :param module_import_name: The __name__ attribute from the calling module
    :return: A string path to the module's location
    """
    module_path_components = module_import_name.split(".")[:2]
    absolute_path_components = [str(x) for x in local_path.split(os.sep)][:-2]
    return os.path.join(os.sep, absolute_path_components[0], os.sep,
                        *(absolute_path_components[1:] + module_path_components))


def get_module_config_from_root_path(path: str) -> dict:
    """
    Gets the module's main config when given the path to the module's root.
    This is a fallback because get_module_config seems to not work if the module is also __main__.
    Root path should always be defined as root_path in a definitions.py in the module's root folder.

    :param path: The module's on-disk path
    :return: A dictionary representing the config.
    """
    with open(os.path.join(path, 'config.json'), 'r') as c:
        return json.load(c)


def get_module_config(module_import_name: str) -> dict:
    """
    Gets the main module config when called inside a module.
    This function assumes the file structure being used in this project, which is to say it assumes that
    the calling module is located inside a sub-folder of the project root.

    :param module_import_name: The __name__ attribute of the module. This argument should always be __name__.
    :return: A dictionary representing the config.
    """
    mod_path = __get_module_path_from_name(module_import_name)
    with open(os.path.join(mod_path, 'config.json'), 'r') as c:
        return json.load(c)


def get_file_config(file_path: str) -> dict:
    """
    Gets the config local to the calling file, which has the name filename.config.json.

    :param file_path: The name of the calling file. This can be determined using os.path.basename(__file__).
    :return: A dictionary representing the config.
    """
    with open(os.path.join(file_path + '.config.json'), 'r') as c:
        return json.load(c)
