The repository for the *new* Stem Dataops applications, including EC2 deployments and other scripts. Currently in a test state, eventually this repository will be moved and renamed.

- `install.py` is used to add the main directory repo to the PYTHONPATH, which allows all the imports to work.
- `applications/` contains application specific code in python modules that are run through calling their `main.py`.
- `common/` contains library code in python modules for use in applications.
- `containers/` contains various docker container configurations which can be built by running `./release.sh` through a terminal, and scheduled to run using `crontab` and `auto_run.sh`.

This repository follows the majority of the PEP 8 style guide, as well as several other general conventions:

1. Code should be separated into distinct modules for organization so that behaviors can easily be found for fixing and reuse. Modules should contain everything they need for configuration and use in their directory.
2. Code documentation should be done primarily through docstrings. Inline comments should be kept to a minimum, ideally not used at all. Code should be legible without comments, using expressive names.
3. Functions should be kept short, and all repeated code should be turned into functions if possible. The more lines a function has, the higher the likelihood that it can be split up.
4. Functions should perform one action. If a function's high level purpose cannot be described in one to two short sentences, it should be split up.
5. Functions which are not intended to be used outside a given module should have their name prefixed with two underscores ex: `__get_token`.
6. Functions should return either one value, or a tuple of closely-related values. If complex information must be returned, consider splitting the function or making a class.
7. Functions should have, at most, two return types: One primary return type, and None in some cases where it may be valid.
8. Names of variables and functions should be pythonic, which is to say they use a word_word2_word3 format, rather than a wordWord2Word3 format or other similar formats.
9. Configuration files should be in json format, and the primary configuration file should be called `config.json` for each module.
10. Container definitions should only deploy modules which are needed to run their application.
11. Container definition directories should contain, at a minimum: A Dockerfile, a `release.sh`, and a `requirements.txt` file for use with `pip`.
12. Packages should have a `definitions.py` file in their root which contains any package-level variables which are needed for operation such as the package's root path.