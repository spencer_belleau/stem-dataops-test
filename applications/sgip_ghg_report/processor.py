import datetime
import calendar
import openpyxl
import openpyxl.drawing.image
import os
import pandas as pd
import applications.sgip_ghg_report.definitions as local_definitions
import common.config_tools.manager as config_manager
import common.salesforce_api.get as sf_get
import common.grovestreams_api.dataframe_api as gst_api
import common.xlsx_reporter.dataframe_printer as xlsx_printer
import common.xlsx_reporter.worksheet_manipulator as ws_funcs

pd.options.mode.chained_assignment = None  # Disable false positive chained assignment warns
local_path = os.path.dirname(os.path.abspath(__file__))
config = config_manager.get_module_config_from_root_path(local_definitions.root_path)


def create_empty_default_workbook():
    """
    Creates a workbook and removes the default sheet.

    :return: An empty workbook.
    """
    wb = openpyxl.workbook.Workbook()
    wb.remove(wb.worksheets[0])
    return wb


def append_worksheet(wb: openpyxl.Workbook, sheet_name: str):
    """
    Appends a worksheet at the last index in a workbook.

    :param wb: The workbook to create the sheet for.
    :param sheet_name: The name of the sheet.
    """
    ws = wb.create_sheet(sheet_name)
    ws.sheet_view.zoomScale = 85
    return ws


def insert_worksheet(wb: openpyxl.Workbook, sheet_name: str, index: int):
    """
    Inserts a worksheet at the given index into a workbook.

    :param wb: The workbook to create the sheet for.
    :param sheet_name: The name of the sheet.
    :param index: The index to insert into.
    """
    ws = wb.create_sheet(sheet_name, index)
    ws.sheet_view.zoomScale = 85
    return ws


def __calculate_compliance(row: pd.Series, number_of_months: float = 0, row_key: str = '') -> str:
    """
    Using the given metric from https://stemedge.atlassian.net/browse/UPM-8742, calculate the SGIP Compliance.

    :param row: The row to calculate the compliance on.
    :return: A string representing whether compliance was met.
    """
    if row[row_key] <= (config['SGIP_GHG_Target'] * number_of_months / 12):
        return "Meets Expectations"
    else:
        return "Below Expectations"


def __calculate_compliance_months_key(row: pd.Series, number_of_months_key: str = '', row_key: str = '') -> str:
    if row[row_key] <= (config['SGIP_GHG_Target'] * row[number_of_months_key] / 12):
        return "Meets Expectations"
    else:
        return "Below Expectations"


def __calculate_penalty(row: pd.Series, number_of_months: float = 0) -> float:
    """
    Using the given metric from https://stemedge.atlassian.net/browse/UPM-8742, calculate the SGIP Penalty.

    :param row: The row to calculate the penalty on.
    :return: The float value of the penalty.
    """
    if row['CO2/kWh'] <= (config['SGIP_GHG_Target'] * number_of_months / 12):
        return 0.0
    else:
        return ((float(config['SGIP_GHG_Target']) * number_of_months / 12.0) - row['CO2/kWh']) \
               * row['SGIP_Energy_kWh__c']


def __format_system_dataframe_columns(df: pd.DataFrame) -> pd.DataFrame:
    """
    Formats the system dataframe columns for printing.

    :param df: The system dataframe
    :return: A dataframe which can be printed in the correct format.
    """
    df = df[['System Name', 'System ID', 'SGIP_Power_kW__c', 'SGIP_Energy_kWh__c', 'PBI_Start_Date__c',
             'Marginal GHG Emissions', 'CO2/kWh', 'Compliance', 'Penalty', 'Annual Payment', 'Percent of Payment']]
    df['SGIP_Power_kW__c'] = df['SGIP_Power_kW__c'].apply(ws_funcs.format_two_decimal_places)
    df['SGIP_Energy_kWh__c'] = df['SGIP_Energy_kWh__c'].apply(ws_funcs.format_two_decimal_places)
    df['Marginal GHG Emissions'] = df['Marginal GHG Emissions'].apply(ws_funcs.format_two_decimal_places)
    df['CO2/kWh'] = df['CO2/kWh'].apply(ws_funcs.format_two_decimal_places)
    df['Penalty'] = df['Penalty'].apply(ws_funcs.format_currency)
    df['Annual Payment'] = df['Annual Payment'].apply(ws_funcs.format_currency)
    df['Percent of Payment'] = df['Percent of Payment'].apply(ws_funcs.format_percent_100)
    df['PBI_Start_Date__c'] = pd.to_datetime(df['PBI_Start_Date__c'], utc=True).dt.strftime('%Y-%m-%d')
    df.loc[df['PBI_Start_Date__c'] == 'NaT', 'PBI_Start_Date__c'] = ''
    df = df.rename({
        'SGIP_Power_kW__c': 'SGIP Power (kW)',
        'SGIP_Energy_kWh__c': 'SGIP Energy (kWh)',
        'PBI_Start_Date__c': 'PBI Start Date',
        'Marginal GHG Emissions': 'Marginal GHG Emissions (kgCO2) - \nCurrent Operation Year to Date',
        'CO2/kWh': 'kg CO2/kWh - \nCurrent Operation Year to Date',
        'Compliance': 'GHG Compliance Status - \nCurrent Operation Year to Date',
        'Penalty': 'Potential Penalty - \nCurrent Operation Year to Date',
        'Annual Payment': 'Annual PBI Payment',
        'Percent of Payment': '% of Annual Payment'
    }, axis='columns')
    return df


def __format_monthly_dataframe_columns(df: pd.DataFrame) -> pd.DataFrame:
    """
    Formats the monthly dataframe columns for printing.

    :param df: The monthly dataframe
    :return: A dataframe which can be printed in the correct format.
    """
    df = df[['datetime', 'data', 'Avg kgCO2/kWh', 'GHG Compliance Status']]
    df['data'] = df['data'].apply(ws_funcs.format_two_decimal_places)
    df['Avg kgCO2/kWh'] = df['Avg kgCO2/kWh'].apply(ws_funcs.format_two_decimal_places)
    df = df.rename({
        'datetime': 'Month',
        'data': 'Marginal GHG Emissions (kgCO2)'
    }, axis='columns')
    df['Month'] = df['Month'].dt.strftime('%Y-%m')
    return df


def __format_yearly_dataframe_columns(df: pd.DataFrame) -> pd.DataFrame:
    """
    Formats the yearly dataframe columns for printing.

    :param df: The yearly dataframe
    :return: A dataframe which can be printed in the correct format.
    """
    df = df[['datetime', 'data', 'Avg kgCO2/kWh', 'GHG Compliance Status']]
    df['data'] = df['data'].apply(ws_funcs.format_two_decimal_places)
    df['Avg kgCO2/kWh'] = df['Avg kgCO2/kWh'].apply(ws_funcs.format_two_decimal_places)
    df = df.rename({
        'datetime': 'Year',
        'data': 'Marginal GHG Emissions (kgCO2)'
    }, axis='columns')
    return df[::-1]


def __anniversary_date(dt: datetime.datetime, target: datetime.datetime) -> datetime.datetime:
    """
    Given a datetime and a target date, finds the last anniversary of the datetime.

    :param dt: The date to turn into an anniversary.
    :param target: The target to measure the anniversary from.
    :return: A Python datetime for the anniversary.
    """
    span = target - dt
    if span < datetime.timedelta(days=365):
        return dt
    else:
        return dt.replace(year=target.year)


def __pbi_column_dt(col: pd.Series) -> pd.Series:
    """
    Converts a column in a dataframe of PBI date strings to Python datetimes.

    :param col: The column to covert.
    :return: A column of Python datetimes.
    """
    return col.apply(__pbi_date_to_datetime)


def __pbi_date_to_datetime(pbi: str) -> datetime.datetime:
    """
    Turns the PBI date string into a python datetime.

    :param pbi: The PBI date string.
    :return: A Python datetime.
    """
    return datetime.datetime.strptime(pbi, "%Y-%m-%d").replace(tzinfo=datetime.timezone.utc)


def __subtract_one_month(dt: datetime.datetime) -> datetime.datetime:
    """
    Subtracts one month from a datetime. Used for fixing the marginal emission data dates.

    :param dt: The datetime to modify
    :return: A datetime exactly one month before dt
    """
    if dt.month > 1:
        days = calendar.monthrange(dt.year, dt.month)
        return dt - datetime.timedelta(days=days[1])  # dt.replace(month=dt.month - 1)
    else:
        return dt.replace(year=dt.year - 1, month=12)


def __get_values_with_year(sr: pd.Series, year: int) -> int:
    """
    Counts the number of datetimes with the given year in a pandas series.

    :param sr: The pandas series, assumed to be datetimes.
    :param year: The year to look for.
    :return: A count of the number of series elementsthat match the given year.
    """
    c = sr[sr.dt.year == year]
    return len(c)


def __fetch_monthly_emission_data(sid: str, sf_df: pd.DataFrame,
                                  end_date: datetime.datetime) -> pd.DataFrame:
    """
    Gets the monthly emission data between the last PBI Anniversary and the report date from GroveStreams.
    Grovestreams data is indexed by end date, so the datetimes are shifted accordingly.

    :param sid: The system ID to search.
    :param sf_df: The salesforce information dataframe.
    :param end_date: The date of the report.
    :return: A dataframe with one row per month in the reporting period containing the requested emission data.
    """
    gst_component_name = f"{sid}_GHG_Calc"
    pbi_start_date = sf_df.iloc[0]['PBI_Start_Date__c']
    marginal_data = gst_api.get_component_stream(gst_component_name, "EmissionMarginalkg_Monthly",
                                                 pbi_start_date - datetime.timedelta(days=31),
                                                 end_date + datetime.timedelta(days=1))
    marginal_data['datetime'] = marginal_data['time'].apply(gst_ts_to_dt_minus_one_month)
    # marginal_data['datetime'] = marginal_data['datetime'].apply(__subtract_one_month)
    marginal_data = marginal_data[marginal_data['datetime'] >= pbi_start_date]
    marginal_data = marginal_data[marginal_data['datetime'] <= end_date]
    return marginal_data


def __fetch_5min_emission_data(sid: str, sf_df: pd.DataFrame, end_date: datetime.datetime) -> pd.DataFrame:
    """
    Gets the 5-minute emission data between the last PBI Anniversary and the report date from GroveStreams.

    :param sid: The system ID to search.
    :param sf_df: The salesforce information dataframe.
    :param end_date: The date of the report.
    :return: A dataframe with the requested emission data in 5-minute intervals.
    """
    gst_component_name = f"{sid}_GHG_Calc"
    pbi_anniversary = __anniversary_date(sf_df.iloc[0]['PBI_Start_Date__c'], end_date)
    marginal_data = gst_api.get_5min_data_between_dates(gst_component_name, "EmissionMarginalkg_5min", pbi_anniversary,
                                                        end_date)
    marginal_data['datetime'] = marginal_data['time'].apply(gst_ts_to_dt)
    marginal_data = marginal_data[marginal_data['datetime'] <= end_date]
    marginal_data = marginal_data[marginal_data['datetime'] >= pbi_anniversary]
    return marginal_data


def __fetch_relevant_salesforce_info(sid: str) -> pd.DataFrame:
    """
    Queries salesforce for the required information for generating the SGIP reports.

    :param sid: The system ID to search for.
    :return: A dataframe with one row for the active SGIP Application.
    """
    access = sf_get.get_access_info()
    system_query = f"SELECT Name, Case_Safe_ID_System__c FROM System__c WHERE System_Record_ID__c = '{sid}'"
    system_result = sf_get.get_records_df_query_default_access(access, system_query)
    system_name = system_result.iloc[0]['Name']
    case_safe_id = system_result.iloc[0]['Case_Safe_ID_System__c']
    sf_query = f"SELECT SGIP_Power_kW__c, SGIP_Energy_kWh__c, PBI_Start_Date__c, PBI_Amount__c " \
               f"FROM SGIP_Application__c WHERE System__c = '{case_safe_id}' AND SGIP_Application_Status__c " \
               f"in ('Initial Incentive Paid - PBI In-Progress', 'Full Incentive Paid')"
    sf_df = sf_get.get_records_df_query_default_access(access, sf_query)
    sf_df['PBI_Start_Date__c'] = __pbi_column_dt(sf_df['PBI_Start_Date__c'])
    sf_df['System Name'] = system_name
    sf_df['System ID'] = sid
    return sf_df


def create_system_dataframe(sf_df: pd.DataFrame, marginal_data: pd.DataFrame, date: datetime.datetime) -> pd.DataFrame:
    """
    Creates a site's system info dataframe using marginal data from Grovestreams and the System info from salesforce.

    :param sf_df: The salesforce information dataframe.
    :param marginal_data: The marginal data from grovestreams.
    :param date: The report date
    :return: The system information dataframe.
    """
    marginal_data = marginal_data[
        marginal_data['datetime'] >= __anniversary_date(pd.to_datetime(sf_df['PBI_Start_Date__c']).iloc[0], date)
    ]
    sf_df['Marginal GHG Emissions'] = marginal_data['data'].sum()
    sf_df['CO2/kWh'] = sf_df['Marginal GHG Emissions'] / sf_df['SGIP_Energy_kWh__c']
    sf_df['Compliance'] = sf_df.apply(__calculate_compliance, number_of_months=len(marginal_data), row_key='CO2/kWh',
                                      axis='columns')
    sf_df['Penalty'] = sf_df.apply(__calculate_penalty, number_of_months=len(marginal_data), axis='columns')
    sf_df['Annual Payment'] = sf_df['PBI_Amount__c'] / 5
    sf_df['Percent of Payment'] = sf_df['Penalty'] / sf_df['Annual Payment']
    return sf_df


def gst_ts_to_dt(ts: int) -> datetime.datetime:
    """
    Converts a grovestreams timestamp to a datetime.

    :param ts: The timestamp.
    :return: A python datetime object.
    """
    return datetime.datetime.fromtimestamp(ts / 1000).replace(tzinfo=datetime.timezone.utc)


def gst_ts_to_dt_minus_one_month(ts: int) -> datetime.datetime:
    """
    Converts a grovestreams timestamp to a datetime.

    :param ts: The timestamp.
    :return: A python datetime object.
    """
    return __subtract_one_month(datetime.datetime.fromtimestamp(ts / 1000).replace(tzinfo=datetime.timezone.utc))


def create_monthly_info_dataframe(sf_df: pd.DataFrame, marginal_data: pd.DataFrame) -> pd.DataFrame:
    """
    Creates a site's monthly info dataframe using marginal data from Grovestreams and the SGIP Energy from Salesforce.

    :param sf_df: The salesforce information dataframe.
    :param marginal_data: The marginal data from grovestreams.
    :return: The monthly information dataframe.
    """
    marginal_data['Avg kgCO2/kWh'] = marginal_data['data'] / sf_df.iloc[0]['SGIP_Energy_kWh__c']
    marginal_data['GHG Compliance Status'] = marginal_data.apply(__calculate_compliance, number_of_months=1,
                                                                 row_key='Avg kgCO2/kWh', axis='columns')
    return marginal_data


def create_yearly_info_dataframe(sf_df: pd.DataFrame, marginal_data: pd.DataFrame, date: datetime.datetime) \
        -> pd.DataFrame:
    """
    Creates a site's yearly info dataframe using marginal data from Grovestreams and the SGIP Energy from Salesforce.

    :param sf_df: The salesforce information dataframe.
    :param marginal_data: The marginal data from grovestreams.
    :param date: The report date
    :return: The yearly information Dataframe.
    """
    marginal_data = marginal_data[
        marginal_data['datetime'] >= __anniversary_date(pd.to_datetime(sf_df['PBI_Start_Date__c']).iloc[0], date)]
    marginal_data['months'] = 1
    marginal_data = marginal_data.set_index('datetime')
    marginal_data = marginal_data.groupby(marginal_data.index.year).sum()
    marginal_data = marginal_data.reset_index()
    marginal_data['Avg kgCO2/kWh'] = marginal_data['data'] / sf_df.iloc[0]['SGIP_Energy_kWh__c']
    marginal_data['GHG Compliance Status'] = marginal_data.apply(__calculate_compliance_months_key,
                                                                 row_key='Avg kgCO2/kWh', number_of_months_key='months',
                                                                 axis='columns')
    return marginal_data


def __print_header_information(sheet: openpyxl.workbook.workbook.Worksheet, A1_name: str, start_date: datetime.datetime,
                               end_date: datetime.datetime, sheet_id: str) -> None:
    """
    Prints the exact format of header information used in this report.

    :param sheet: The sheet to print on.
    :param A1_name: The value to put in the A1 cell, usually a site name or an account name
    :param start_date: The start of the reporting period.
    :param end_date: The end of the reporting period.
    :param sheet_id: The sheet id, either an SID or "Portfolio"
    """
    sheet.row_dimensions[1].height = 18
    sheet['A1'] = A1_name
    ws_funcs.set_cell_a1(sheet, 1, 1)
    sheet['A5'] = f"{sheet_id} SGIP Emissions Report"
    ws_funcs.set_cell_bold_text(sheet, 5, 1)
    sheet['A6'] = f"Period: {start_date.strftime('%Y-%m-%d %H:%M')} to {end_date.strftime('%Y-%m-%d %H:%M')}"
    ws_funcs.set_cell_bold_text(sheet, 6, 1)
    sheet['D1'] = "Stem, Inc."
    sheet['D2'] = "100 California St, 14th Floor"
    sheet['D3'] = "San Francisco, CA 94111"
    img = openpyxl.drawing.image.Image(os.path.join(local_path, 'stem_logo.png'))
    img.anchor = 'E1'
    sheet.add_image(img)


def __print_dfs_to_sheet(sheet: openpyxl.workbook.workbook.Worksheet, system_df: pd.DataFrame,
                         monthly_df: pd.DataFrame, yearly_df: pd.DataFrame) -> None:
    """
    Prints the system, monthly, and yearly dataframes to a given sheet. Works for both site sheets and portfolio.

    :param sheet: The worksheet to print to.
    :param system_df: The system information dataframe.
    :param monthly_df: The monthly information dataframe.
    :param yearly_df: The yearly information dataframe.
    """
    row_num = xlsx_printer.write_dataframe_column_headers(
        __format_system_dataframe_columns(system_df), sheet, 1, 8)
    row_num = xlsx_printer.write_dataframe_column_headers(
        __format_yearly_dataframe_columns(yearly_df), sheet, 1, row_num + 2)
    xlsx_printer.write_dataframe_column_headers(
        __format_monthly_dataframe_columns(monthly_df), sheet, 1, row_num + 2)


def __store_site_dataframes(data_dict: dict, system_df: pd.DataFrame, monthly_df: pd.DataFrame,
                            yearly_df: pd.DataFrame, interval_df: pd.DataFrame) -> dict:
    """
    Stores the site dataframes for system, monthly, yearly, and 5-minute interval information in the data dictionary.

    :param data_dict: The data dictionary.
    :param system_df: The system information dataframe.
    :param monthly_df: The monthly information dataframe.
    :param yearly_df: The yearly information dataframe.
    :param interval_df: The 5-minute interval data dataframe
    :return: The data dictionary with new information appended.
    """
    data_dict['system'].append(system_df)
    data_dict['monthly'].append(monthly_df)
    data_dict['yearly'].append(yearly_df)
    data_dict['interval'].append(interval_df)
    return data_dict


def __create_site_worksheet(workbook: openpyxl.workbook.Workbook, sid: str, date: datetime.datetime,
                            sheet_data: dict) -> dict:
    """
    Creates a site worksheet and inserts it into the provided workbook.
    Also computes all data necessary for the sheet and fills it.

    :param workbook: The workbook to insert the sheet into.
    :param sid: The SID of the site associated with the sheet.
    :param date: The date of the report.
    :param sheet_data: A dictionary used for storing the relevant dataframes, used in the construction of the portfolio.
    :return: sheet_data, with the newly calculated information added.
    """
    worksheet = append_worksheet(workbook, sid + "_GHG")
    sf_df = __fetch_relevant_salesforce_info(sid)
    marginal_df = __fetch_monthly_emission_data(sid, sf_df, date)
    interval_df = __fetch_5min_emission_data(sid, sf_df, date)
    system_df = create_system_dataframe(sf_df, marginal_df, date)
    monthly_df = create_monthly_info_dataframe(sf_df, marginal_df)
    yearly_df = create_yearly_info_dataframe(sf_df, marginal_df, date)
    __print_header_information(worksheet, sf_df.iloc[0]['System Name'],
                               __anniversary_date(sf_df.iloc[0]['PBI_Start_Date__c'], date),
                               date, sid)
    __print_dfs_to_sheet(worksheet, system_df, monthly_df, yearly_df)
    sheet_data = __store_site_dataframes(sheet_data, system_df, monthly_df, yearly_df, interval_df)
    __set_column_row_sizes(worksheet)
    return sheet_data


def __create_portfolio_sheet(workbook: openpyxl.workbook.Workbook, client_name: str, date: datetime.datetime,
                             sheet_data: dict) -> None:
    """
    Creates the "portfolio" worksheet, a summary of all the site worksheets, and adds it to the first sheet position.

    :param workbook: The workbook to add the sheet to.
    :param client_name: The name of the client for whom the report is being generated.
    :param date: The date of the report.
    :param sheet_data: The sheet data output of each site worksheet generation.
    """
    site_sum_df = __create_system_sum_row(sheet_data['system'])
    portfolio_system_df = pd.DataFrame(pd.concat([*sheet_data['system'], site_sum_df], sort=True),
                                       columns=sheet_data['system'][0].columns).fillna('')
    yearly_sum_df = __create_yearly_sum_df(sheet_data['yearly'], site_sum_df.iloc[0]['SGIP_Energy_kWh__c'])
    monthly_sum_df = __create_monthly_sum_df(sheet_data['monthly'], site_sum_df.iloc[0]['SGIP_Energy_kWh__c'])
    portfolio_worksheet = insert_worksheet(workbook, "Portfolio_GHG", 0)
    __print_header_information(portfolio_worksheet, client_name,
                               __anniversary_date(portfolio_system_df.iloc[0]['PBI_Start_Date__c'], date),
                               date, "Portfolio")
    __print_dfs_to_sheet(portfolio_worksheet, portfolio_system_df, monthly_sum_df, yearly_sum_df)
    __set_column_row_sizes(portfolio_worksheet)


def __create_blank_portfolio_sum_dataframe() -> pd.DataFrame:
    """
    Creates a blank portfolio sum dataframe with appropriate columns. Used for making the bottom row of the
    portfolio system dataframe.

    :return: A dataframe with the specified columns and 0 values.
    """
    return pd.DataFrame({
        'SGIP_Power_kW__c': [0],
        'SGIP_Energy_kWh__c': [0],
        'Marginal GHG Emissions': [0],
        'Penalty': [0],
        'Annual Payment': [0],
        'Percent of Payment': [0]
    })


def __create_system_sum_row(system_dfs: list) -> pd.DataFrame:
    """
    Constructs the bottom row for the portfolio system dataframe.

    :param system_dfs: The list of system information dataframes
    :return: A dataframe with one row to be appended to the bottom of the portfolio system dataframe
    """
    site_sum_df = __create_blank_portfolio_sum_dataframe()
    for site in system_dfs:
        add_df = site.loc[:, site.columns != 'PBI_Start_Date__c']
        site_sum_df = site_sum_df + add_df
    site_sum_df['System Name'] = 'Portfolio Sum'
    return site_sum_df


def __create_yearly_sum_df(yearly_dfs: list, energy_sum: int) -> pd.DataFrame:
    """
    Takes the list of yearly information dataframes and constructs a portfolio yearly dataframe from it.

    :param yearly_dfs: The list of yearly information dataframes.
    :param energy_sum: The sum of the SGIP Energy (kWh) rows from the portfolio system dataframe
    :return: A portfolio yearly dataframe
    """
    yearly_sum_df = yearly_dfs[0].copy(deep=True)
    for i in range(1, len(yearly_dfs)):
        yearly_sum_df['data'] = yearly_sum_df['data'].add(yearly_dfs[i]['data'], fill_value=0.0)
    yearly_sum_df['Avg kgCO2/kWh'] = yearly_sum_df['data'] / energy_sum
    yearly_sum_df['GHG Compliance Status'] = yearly_sum_df.apply(__calculate_compliance_months_key,
                                                                 number_of_months_key='months',
                                                                 row_key='Avg kgCO2/kWh', axis='columns')
    return yearly_sum_df


def __create_monthly_sum_df(monthly_dfs: list, energy_sum: int) -> pd.DataFrame:
    """
    Takes the list of monthly information dataframes and constructs a portfolio monthly dataframe from it.

    :param monthly_dfs: The list of monthly information dataframes.
    :param energy_sum: The sum of the SGIP Energy (kWh) rows from the portfolio system dataframe
    :return: A portfolio monthly dataframe
    """
    monthly_sum_df = monthly_dfs[0].copy(deep=True)
    for i in range(1, len(monthly_dfs)):
        monthly_sum_df['data'] = monthly_sum_df['data'].add(monthly_dfs[i]['data'], fill_value=0.0)
    monthly_sum_df['Avg kgCO2/kWh'] = monthly_sum_df['data'] / energy_sum
    monthly_sum_df['GHG Compliance Status'] = monthly_sum_df.apply(__calculate_compliance, number_of_months=1,
                                                                   row_key='Avg kgCO2/kWh', axis='columns')
    return monthly_sum_df


def __set_column_row_sizes(ws: openpyxl.workbook.workbook.Worksheet) -> None:
    """
    Sets the column widths of the sheet to some pre-decided values based on the template.
    Sets the height of row 8 to fit two lines.

    :param ws: The worksheet to set the widths on.
    """
    ws.row_dimensions[8].height = 28
    ws.column_dimensions['A'].width = 28
    ws.column_dimensions['B'].width = 35
    ws.column_dimensions['C'].width = 25
    ws.column_dimensions['D'].width = 28
    ws.column_dimensions['E'].width = 28
    ws.column_dimensions['F'].width = 35
    ws.column_dimensions['G'].width = 28
    ws.column_dimensions['H'].width = 35
    ws.column_dimensions['I'].width = 28
    ws.column_dimensions['J'].width = 28
    ws.column_dimensions['K'].width = 28


def __create_interval_data_sheet(wb: openpyxl.workbook.Workbook, sids: list, dfs: list) -> None:
    """
    Adds an interval data worksheet to a workbook in the second sheet position.

    :param wb: The workbook to add the sheet to.
    :param sids: The list of Site IDs (for use in column name generation)
    :param dfs: The list of interval data dataframes.
    """
    interval_df = dfs[0][['datetime', 'data']]
    interval_df = interval_df.rename({'data': f'{sids[0]}_GHG_Calc.EmissionMarginalkg_5min'}, axis='columns')
    for i in range(1, len(dfs)):
        interval_df[f'{sids[i]}_GHG_Calc.EmissionMarginalkg_5min'] = dfs[i]['data']
    interval_df.reset_index()
    interval_df = interval_df.rename({'datetime': 'Time Start'}, axis='columns').fillna(0.0)
    sheet = insert_worksheet(wb, "Interval_Data", 1)
    interval_df[interval_df.columns[1]] = interval_df[interval_df.columns[1]] \
        .apply(ws_funcs.format_two_decimal_places)
    interval_df[interval_df.columns[2]] = interval_df[interval_df.columns[2]] \
        .apply(ws_funcs.format_two_decimal_places)
    xlsx_printer.write_dataframe_column_headers(interval_df.sort_values(by='Time Start'), sheet, 1, 1)
    sheet.column_dimensions['A'].width = 30
    sheet.column_dimensions['B'].width = 40
    sheet.column_dimensions['C'].width = 40
