import argparse
import datetime
import os
import sys
import applications.sgip_ghg_report.processor as processor
import applications.sgip_ghg_report.definitions as local_definitions
import common.config_tools.manager as config_manager

local_path = os.path.dirname(os.path.abspath(__file__))
config = config_manager.get_module_config_from_root_path(local_definitions.root_path)


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--client", type=str, required=True,
                    help="The name of the client associated with the report. If client name contains spaces,"
                    "enclose argument in double quotes. Ex: -c \"Sungreen Systems\"")
parser.add_argument("-s", "--site_ids", nargs='+', required=True,
                    help="The list of Site IDs to process for the report. Ex: -s S-0012410 S-0012412")
parser.add_argument("-d", "--date", type=str, required=True,
                    help="The date, in format yyyy-mm-dd, of the report. Ex: -d 2022-04-30")


def main(client_name: str, sids: list, date: datetime.datetime) -> None:
    """
    Produce a client report for the given SIDs and date.

    :param client_name: The client name, to be printed in the A1 cells of the report sheets
    :param sids: The SIDs associated with the client's systems
    :param date: The date of the report.
    """
    workbook = processor.create_empty_default_workbook()
    sheet_data = {'system': [], 'monthly': [], 'yearly': [], 'interval': []}
    for sid in sids:
        sheet_data = processor.__create_site_worksheet(workbook, sid, date, sheet_data)
    processor.__create_portfolio_sheet(workbook, client_name, date, sheet_data)
    processor.__create_interval_data_sheet(workbook, sids, sheet_data['interval'])
    workbook.save(os.path.join(local_path,
                               f"{client_name}_{date.strftime('%m_%Y')}_SGIP_"
                               f"GHG_prelim_{datetime.datetime.now().strftime('%Y_%m_%d')}.xlsx"))


if __name__ == '__main__':
    args = parser.parse_args()
    if not args.client:
        print("No client name set.")
        sys.exit(-1)
    if not args.site_ids:
        print("No site ids set.")
        sys.exit(-1)
    if not args.date:
        print("No date selected.")
        sys.exit(-1)
    target_date = datetime.datetime.strptime(args.date, "%Y-%m-%d")
    main(args.client, args.site_ids, datetime.datetime(target_date.year, target_date.month, target_date.day,
                                                       23, 55, tzinfo=datetime.timezone.utc))
