import argparse
import applications.ghg_watttime.fetch as bridge


parser = argparse.ArgumentParser()
parser.add_argument("-m", "--marginal_data", action="store_true",
                    help="Run the Marginal Emission Data fetch operation.")
parser.add_argument("-f", "--forecast", action="store_true",
                    help="Run the Emission Forecast Data fetch operation")
parser.add_argument("-t", "--historical", action="store_true",
                    help="Run the Historical Emission Data fetch operation")
parser.add_argument("-r", "--range", type=int,
                    help="Number of hours to look back when fetching data. Only used with -m/--marginal_data."
                         "If excluded, defaults to 25 hours.")
parser.add_argument("-ba", "--balancing_authority", type=str, nargs='+',
                    help="The Balancing Authority codes to process. "
                         "If excluded, all Balancing Authorities will be processed.")
parser.set_defaults(hours=25)


def main() -> None:
    args = parser.parse_args()
    if not (args.forecast or args.marginal_data or args.historical):
        print("No mode selected.")
    if args.marginal_data:
        bridge.fetch_marginal_emissions_data(args.hours, args.balancing_authority)
    if args.forecast:
        bridge.fetch_all_forecast_data(args.balancing_authority)
    if args.historical:
        bridge.fetch_historical_marginal_data(args.balancing_authority)


if __name__ == '__main__':
    main()
