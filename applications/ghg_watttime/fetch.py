import pandas as pd
import datetime
import pytz
import os
import shutil
import common.watttime_api.api as wt_api
import common.grovestreams_api.dataframe_api as gst_api
import common.config_tools.manager as config_manager
import applications.ghg_watttime.definitions as local_definitions
from zipfile import ZipFile

local_path = os.path.dirname(os.path.abspath(__file__))
config = config_manager.get_module_config_from_root_path(local_definitions.root_path)


def __get_token(credentials: tuple = None) -> str:
    """
    Gets a valid watttime_api token for use in other watttime_api calls.

    :param credentials: A tuple of (username, password) to use as login information, optional
    :return: A string representation of the token.
    """
    if credentials is not None:
        return wt_api.login_with_credentials(credentials[0], credentials[1])
    else:
        return wt_api.login()


def __dt_format(dt: datetime.datetime) -> str:
    """
    Formats a datetime object for use with the watttime_api
    Currently unused because it turns out that it's not necessary

    :param dt: the datetime object
    :rtype: str
    :return: A string of the date in format yyyy-mm-ddThh:MM:ss-zone
    """
    return dt.strftime("%Y-%m-%dT%H:%M:%S%z")


def __write_and_extract_zip(file_bytes: bytes, file_path: str, extract_path: str) -> None:
    """
    Writes a zip file's bytes to a location, then extracts the contents to a directory, then deletes the zip file.

    :param file_bytes: The bytes making up the zip file.
    :param file_path: The location to create the zip file.
    :param extract_path: The location to extract the zip file's contents to.
    """
    with open(file_path, 'wb') as archive:
        archive.write(file_bytes)
    with ZipFile(file_path, 'r') as archive:
        archive.extractall(extract_path)
    if os.path.exists(file_path):
        os.remove(file_path)


def __get_balancing_authority_list(bas_to_process: list = None) -> dict:
    """
    Reads the Balancing Authorities json into a python dictionary
    If given a specific Balancing Authority, will retrieve just that Balancing Authority's information

    :param bas_to_process: The Balancing Authority list to process exclusively, optional
    :rtype: dict
    :return: A dict representing the json file
    """
    ba_list = config["Balancing_Authorities"]
    if bas_to_process is None:
        return ba_list
    elif bas_to_process is not None:
        for ba in bas_to_process:
            if ba not in ba_list.keys():
                raise KeyError("Unknown Balancing Authority: " + ba)
        ba_list = dict((key, ba_list[key]) for key in bas_to_process)
        return ba_list
    else:
        return ba_list


def __upload_data(json_data: dict, component: str, stream: str) -> None:
    """
    Process and upload the data from the watttime_api call to GroveStreams

    :param json_data: The JSON response object's data list
    :param component: The name of the target GroveStreams component
    :param stream: The name of the target stream
    """
    rows = []
    for obj in json_data:
        row = {
            'starttime': pd.to_datetime(obj["point_time"]),
            'value': obj['value']
        }
        rows.append(row)
    df = pd.DataFrame(rows)
    df['CompID'] = component
    df['StreamID'] = stream
    retry_count = 0
    uploaded = False
    while not uploaded and retry_count < 10:
        uploaded = gst_api.upload_dataframe(df)
    if not uploaded:
        print("Failed to upload Dataframe, GroveStreams may be down.")


def __upload_historical_csv(path: str, component: str) -> None:
    """
    Uploads information from a historical csv file fetched using watttime_api to the given component.

    :param path: The path to the csv file.
    :param component: The name of the component.
    """
    df_csv = pd.read_csv(path)
    df_csv['CompID'] = component
    df_csv['StreamID'] = 'Marginal_Emission'
    df_csv.rename(columns={'timestamp': 'starttime', 'MOER': 'value'}, inplace=True)
    df_csv['starttime'] = pd.to_datetime(df_csv['starttime'], format='%Y-%m-%dT%H:%M:%S%z')
    df_csv = df_csv[['CompID', 'StreamID', 'starttime', 'value']]
    retry_count = 0
    uploaded = False
    while not uploaded and retry_count < 10:
        uploaded = gst_api.upload_dataframe(df_csv)
    if not uploaded:
        print("Failed to upload Dataframe, GroveStreams may be down.")


def fetch_all_forecast_data(ba_to_process: list = None, credentials: tuple = None) -> None:
    """
    Fetches 24 hours of 5-minute emissions forecast data for all Balancing Authorities,
    then uploads them to GroveStreams

    :param ba_to_process: The Balancing Authority list to process exclusively, optional
    :param credentials: A tuple of (username, password) to use as login information, optional
    """
    token = __get_token(credentials)
    ba_list = __get_balancing_authority_list(ba_to_process)
    for ba in ba_list.keys():
        print("Fetching Forecast data for Balancing Authority: " + ba)
        data = wt_api.emissions_forecast(token, ba)
        if 'error' in data:
            print("Error: Could not fetch forecast data for " + ba)
            print(data['message'])
            continue
        __upload_data(data['forecast'], ba_list[ba]["Component"], 'Forecast')


def fetch_marginal_emissions_data(fetch_range: int, ba_to_process: list = None, credentials: tuple = None) -> None:
    """
    Uploads Marginal Emission data from all Balancing Authorities to GroveStreams.
    Measures from fetch_range hours before call time.

    :param fetch_range: The number of hours to look back when fetching data.
    :param ba_to_process: The Balancing Authority list to process exclusively, optional
    :param credentials: A tuple of (username, password) to use as login information, optional
    """
    token = __get_token(credentials)
    ba_list = __get_balancing_authority_list(ba_to_process)
    for ba in ba_list.keys():
        print("Fetching Marginal Emission data for Balancing Authority: " + ba)
        end_time = datetime.datetime.now(tz=pytz.UTC)
        start_time = end_time - datetime.timedelta(hours=fetch_range)
        data = wt_api.grid_emissions_data(token, ba, start_time, end_time)
        if 'error' in data:
            print("Error: Could not fetch forecast data for " + ba)
            print(data['message'])
            continue
        __upload_data(data, ba_list[ba]["Component"], 'Marginal_Emission')


def fetch_marginal_emissions_data_between_dates(start_time: datetime.datetime, end_time: datetime.datetime,
                                                ba_to_process: list = None, credentials: tuple = None) -> None:
    """
    Uploads Marginal Emission data from all Balancing Authorities to GroveStreams.
    Measures from start_time to end_time.

    :param start_time: When to start measuring data.
    :param end_time: When to stop measuring data.
    :param ba_to_process: The Balancing Authority list to process exclusively, optional
    :param credentials: A tuple of (username, password) to use as login information, optional
    """
    token = __get_token(credentials)
    ba_list = __get_balancing_authority_list(ba_to_process)
    for ba in ba_list.keys():
        print("Fetching Marginal Emission data for Balancing Authority: " + ba)
        interval_start = start_time
        interval_end = start_time + datetime.timedelta(hours=24)
        while True:
            if interval_end > end_time:
                interval_end = end_time
            data = wt_api.grid_emissions_data(token, ba, interval_start, interval_end)
            if 'error' in data:
                print("Error: Could not fetch forecast data for " + ba + ", interval " +
                      str(interval_start) + " to " + str(interval_end))
                print(data['message'])
                continue
            __upload_data(data, ba_list[ba]["Component"], 'Marginal_Emission')
            if interval_end == end_time:
                break
            interval_start = interval_end
            interval_end = interval_start + datetime.timedelta(hours=24)


def fetch_historical_marginal_data(ba_to_process: list = None, credentials: tuple = None) -> None:
    """
    Requests historical data from watttime_api and uses it to populate Marginal_Emission streams.
    Should generally only be run once per BA.

    :param ba_to_process: The Balancing Authority list to process exclusively, optional
    :param credentials: A tuple of (username, password) to use as login information, optional
    """
    ba_list = __get_balancing_authority_list(ba_to_process)
    for ba in ba_list.keys():
        token = __get_token(credentials)
        print("Fetching Historical Emission data for Balancing Authority: " + ba)
        zip_path = os.path.join(local_path, ba + "_historical.zip")
        extract_destination = os.path.join(local_path, ba + "_historical")
        try:
            __write_and_extract_zip(wt_api.historical_emissions(token, ba), zip_path, extract_destination)
            for file_name in os.listdir(extract_destination):
                print("Uploading " + str(file_name))
                __upload_historical_csv(os.path.join(extract_destination, str(file_name)), ba_list[ba]['Component'])
        finally:
            shutil.rmtree(extract_destination, ignore_errors=True)
