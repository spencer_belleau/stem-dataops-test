import os
import datetime
import common.google_api.google_api as gdrive_api
import common.salesforce_api.get as sf_get
import common.config_tools.manager as config_manager
import applications.pg_reprocessing_scheduler.definitions as local_definitions
import warnings
import pandas as pd
from googleapiclient.errors import HttpError

warnings.simplefilter(action='ignore', category=FutureWarning)
local_path = os.path.dirname(os.path.abspath(__file__))
config = config_manager.get_module_config_from_root_path(local_definitions.root_path)

# A large amount of this code is not used. I'm not removing it because it may be useful in the future for
# different processes.


def create_schedule_df() -> pd.DataFrame:
    """
    Queries salesforce for the latest information on True-up terms, and creates a dataframe from them.

    :return: A dataframe with one row per system that has a TUT ending any time past the current date.
    """
    access = sf_get.get_access_info()
    system_query = "SELECT Id, name, Term_Start__c, Term_End__c, System_Rec_Id__c FROM True_Up_Term__c" \
                   " WHERE Term_Status__c NOT IN ('Future', 'Completed') ORDER BY Term_Start__c ASC"
    system_result = sf_get.get_records_df_query_default_access(access, system_query)
    system_result['Term_Start__c'] = pd.to_datetime(system_result['Term_Start__c'])
    system_result['Term_End__c'] = pd.to_datetime(system_result['Term_End__c'])
    system_result = system_result.groupby('System_Rec_Id__c', as_index=False).max()
    start_date = datetime.datetime.utcnow() - datetime.timedelta(days=config['reprocessing_start'])
    end_date = start_date + datetime.timedelta(days=config['reprocessing_span'])
    system_result = system_result[system_result['Term_End__c'] >= start_date]
    system_result = system_result[system_result['Term_End__c'] <= end_date]
    return system_result[['System_Rec_Id__c', 'Name', 'Term_Start__c', 'Term_End__c']]


def get_term_start_end(sid: str) -> dict:
    """
    Given an SID, gets the term start and end of the True-up term for that SID

    :param sid: The SID to search for
    :return: A dictionary with the start and end values.
    """
    access = sf_get.get_access_info()
    system_query = f"SELECT Term_Start__c, Term_End__c FROM True_Up_Term__c WHERE System_Rec_Id__c = '{sid}' " \
                   f"AND Term_Status__c NOT IN ('Future', 'Completed') ORDER BY Term_Start__c ASC"
    system_result = sf_get.get_records_df_query_default_access(access, system_query)
    system_result['Term_Start__c'] = pd.to_datetime(system_result['Term_Start__c'])
    system_result['Term_Start__c'] = system_result['Term_Start__c'] + pd.offsets.DateOffset(months=1)
    system_result['Term_End__c'] = pd.to_datetime(system_result['Term_End__c'])
    return system_result[['Term_Start__c', 'Term_End__c']].to_dict('records')[0]


def save_schedule_to_csv(schedule: pd.DataFrame) -> None:
    """
    Saves the schedule dataframe to a csv file.

    :param schedule: The schedule file.
    """
    schedule.to_csv(os.path.join(local_definitions.root_path, "schedule.csv"), index=False)


def fetch_schedule_from_csv() -> pd.DataFrame:
    """
    Reads a saved schedule csv and returns the dataframe representation of it.

    :return: A schedule dataframe
    """
    return pd.read_csv(os.path.join(local_definitions.root_path, "schedule.csv"))


def create_or_fetch_schedule() -> pd.DataFrame:
    """
    If there is a saved schedule, fetches it. Otherwise, creates a new schedule.

    :return: A schedule dataframe
    """
    if os.path.exists(os.path.join(local_definitions.root_path, "schedule.csv")):
        return fetch_schedule_from_csv()
    else:
        return create_schedule_df()


def rerun_sav_savstem_for_sid(sid: str, path: str) -> None:
    """
    Reruns the PG report for a given SID, using the docker container on the EC2 machines.
    Generates both _Sav and _SavStem folders

    :param sid: The SID to regenerate the report for.
    :param path: The location to save the resulting report to
    """
    run_pg_for_sid(sid + "_Sav", path)
    run_pg_for_sid(sid + "_SavStem", path)


def rerun_sav_savstem_for_sid_2(sid: str, path: str, start: datetime.datetime, end: datetime.datetime) -> None:
    """
    Reruns the PG report for a given SID, using the docker container on the EC2 machines.
    Generates both _Sav and _SavStem folders

    :param sid: The SID to regenerate the report for.
    :param path: The location to save the resulting report to
    :param start: The start datetime for the SID's True-up term
    :param end: The end datetime for the SID's True-up term
    """
    run_pg_for_sid_2(sid + "_Sav", path, start, end)
    run_pg_for_sid_2(sid + "_SavStem", path, start, end)


def run_pg_for_sid(sid: str, path: str) -> None:
    """
    Runs the PG report for a given SID, using the docker container on the EC2 machines.

    :param sid: The SID to regenerate the report for.
    :param path: The location to save the resulting report to
    """
    os.system(f'{config["savings_ops_full_path_command"]} genability_calc.py --component-id "{sid}" --period-id '
              f'"{datetime.datetime.now().year}-01" --no-csv --no-json --no-pdf '
              f'--gs-output {path} '
              f'--gen-output {path} '
              f'--output-csv-dir {path} '
              f'--bill-savings-dir {path}')


def run_pg_for_sid_2(sid: str, path: str, start: datetime.datetime, end: datetime.datetime) -> None:
    """
    Runs the PG report for a given SID, using the docker container on the EC2 machines.

    :param sid: The SID to regenerate the report for.
    :param path: The location to save the resulting report to
    :param start: The start datetime for the True-up term
    :param end: The end datetime for the True-up term
    """
    os.system(f'{config["savings_ops_full_path_command"]} genability_calc.py --component-id "{sid}" '
              f'--from-period "{start.strftime("%Y-%m")}" --to-period "{end.strftime("%Y-%m")}" '
              f'--no-csv '  # --no-json --no-pdf '
              f'--gs-output {path} '
              f'--gen-output {path} '
              f'--output-csv-dir {path} '
              f'--bill-savings-dir {path}')


def move_gdrive_file_with_name_and_parent(filename: str, parent_id: str, new_parent_id: str,
                                          folders_only: bool = False) -> None:
    """
    Moves a file from one location on Google Drive to another location on Google Drive.

    :param filename: The filename, or a long prefix matching the filename.
    :param parent_id: The initial containing folder's id.
    :param new_parent_id: The destination folder's id.
    :param folders_only: Set to true to pass the folders_only flag to google_api
    """
    api = gdrive_api.DriveAPI()
    print(f"Getting ID for drive file with name {filename} in old parent.")
    existing_source_file_id = get_gdrive_file_id(filename, parent_id, api)
    if len(existing_source_file_id) > 0:
        # Check to see if there's an archive folder
        print(f"Try to get ID for drive file with name {filename} in new parent.")
        existing_destination_file_id = get_gdrive_file_id(filename, new_parent_id, api)
        if len(existing_destination_file_id) > 0:
            # Move stuff that's been regenerated to archive
            print(f"Found file with name {filename} in new parent, merging old and new.")
            daily_statements = api.get_file_names(parent_id=existing_source_file_id, folders_only=False)\
                .to_dict('records')
            archived_statements = api.get_file_names(parent_id=existing_destination_file_id, folders_only=False)
            for record in daily_statements:
                print(f"Checking {record['filename']}")
                if record['filename'] in archived_statements['filename'].tolist():
                    print(f"Skipping {record['filename']} because it already exists in new parent.")
                else:
                    print(f"Copying {record['filename']} to new parent.")
                    try_move_file(record['fileid'], existing_destination_file_id, record['filename'], api)
        else:
            # Create Archive Folder
            print(f"No file with name {filename} in new parent, moving from old parent.")
            try_move_file(existing_source_file_id, new_parent_id, filename, api)
    else:
        # Make a daily savings statement folder
        print(f"No files with name {filename} found, cancelling move.\nOther relevant info:\n    "
              f"parent_id: {parent_id}\n    new_parent_id: {new_parent_id}\n    folders_only:{folders_only}")


def do_archive_and_remove_old_statements(filename: str, parent_id: str, new_parent_id: str) -> None:
    """
    Moves a file from one location on Google Drive to another location on Google Drive.

    :param filename: The filename, or a long prefix matching the filename.
    :param parent_id: The initial containing folder's id.
    :param new_parent_id: The destination folder's id.
    """
    api = gdrive_api.DriveAPI()
    print(f"Getting ID for drive file with name {filename} in daily folder.")
    existing_source_file_id = get_gdrive_file_id(filename, parent_id, api)
    if len(existing_source_file_id) > 0:
        print(f"Try to get ID for drive file with name {filename} in archive folder.")
        existing_destination_file_id = get_gdrive_file_id(filename, new_parent_id, api)
        if len(existing_destination_file_id) <= 0:
            print(f"No file with name {filename} in new parent, creating missing archive.")
            existing_destination_file_id = api.create_folder(filename, new_parent_id)
        daily_statements = api.get_file_names(parent_id=existing_source_file_id, folders_only=False) \
            .to_dict('records')
        archived_statements = api.get_file_names(parent_id=existing_destination_file_id, folders_only=False)
        for record in daily_statements:
            print(f"Checking {record['filename']}")
            if record['filename'] in archived_statements['filename'].tolist():
                print(f"Deleting {record['filename']} because it already exists in archive.")
                api.delete_f(record['fileid'])
            else:
                print(f"Copying {record['filename']} to new parent.")
                try_move_file(record['fileid'], existing_destination_file_id, record['filename'], api)
    else:
        api.create_folder(filename, parent_id)
        print(f"No files with name {filename} found, Creating missing Daily Savings Statement Folder"
              f".\nOther relevant info:\n    parent_id: {parent_id}")


def try_move_file(file_id: str, new_parent_id: str, file_name: str, api: gdrive_api.DriveAPI) -> None:
    """
    Tries to move a file on Google Drive using the API, prints an error then passes it on if it fails.

    :param file_id: The id of the file to move.
    :param new_parent_id: The new parent id for the file
    :param file_name: The name of the file, only used for errr message output.
    :param api: A Google Drive API object.
    """
    try:
        api.move_f(file_id=file_id, new_parent=new_parent_id)
    except HttpError as err:
        print(f"Error encountered while attempting to move file {file_name} with id {file_id}."
              f"\nOther relevant info:\n    new_parent_id: {new_parent_id}")
        print(err)
        raise


def archive_or_erase_file(file_id: str, new_parent_id: str, file_name: str, api: gdrive_api.DriveAPI) -> None:
    """
    Tries to move a file on Google Drive using the API. If the file already exists at the destination,
    deletes the source file instead. Will print an error if there is a connection issue.

    :param file_id: The id of the file to move.
    :param new_parent_id: The new parent id for the file
    :param file_name: The name of the file, only used for errr message output.
    :param api: A Google Drive API object.
    """
    try:
        if len(get_gdrive_file_id(file_name, new_parent_id, api)) > 0:
            api.delete_f(file_id)
        else:
            api.move_f(file_id=file_id, new_parent=new_parent_id)
    except HttpError as err:
        print(f"Error encountered while attempting to move file {file_name} with id {file_id}."
              f"\nOther relevant info:\n    new_parent_id: {new_parent_id}")
        print(err)
        raise


def upload_subtree(root_directory: str, root_gdrive_id: str, api=None) -> None:
    """
    Recursively uploads an entire directory subtree to Google Drive.
    Puts the contents of root_directory into root_gdrive_id's folder.

    :param root_directory: The root directory, on disk, to upload.
    :param root_gdrive_id: The root Google Drive object (folder) id to upload the contents to.
    :param api: A handle to the Google Drive API, to be reused
    """
    if api is None:
        api = gdrive_api.DriveAPI()
    subdirectories = []
    for entry in os.scandir(root_directory):
        if entry.is_dir():
            directory_id = api.create_folder(entry.name, root_gdrive_id)
            subdirectories.append((os.path.join(root_directory, entry.name), directory_id))
        elif entry.is_file():
            with open(os.path.join(root_directory, entry.name), 'rb') as f:
                api.write_file(f.read(), entry.name, root_gdrive_id)
    for directory, dir_id in subdirectories:
        upload_subtree(directory, dir_id, api)


def upload_matching_folders(match: str, root_directory: str, root_gdrive_id: str, unmatch: str = None) -> None:
    """
    Uploads folders with names that match (and don't match in the case of unmatch) a given set of substrings to
    the root Google Drive location given.

    :param match: Substring that the folder name must contain.
    :param root_directory: The root directory, on disk, to upload.
    :param root_gdrive_id: The root Google Drive object (folder) id to upload the contents to.
    :param unmatch: Substring that the folder name must NOT contain
    :return:
    """
    api = gdrive_api.DriveAPI()
    for entry in os.scandir(root_directory):
        if unmatch is not None and unmatch in entry.name:
            continue
        if entry.is_dir() and match in entry.name:
            sub_root = api.create_folder(entry.name, root_gdrive_id)
            upload_subtree(os.path.join(root_directory, entry.name), sub_root)


def archive_existing_statements_and_replace(match: str, root_directory: str, daily_gdrive_id: str,
                                            archive_gdrive_id: str, unmatch: str = None) -> None:
    """
    Uploads folders with names that match (and don't match in the case of unmatch) a given set of substrings to
    the root Google Drive location given.

    :param match: Substring that the folder name must contain.
    :param root_directory: The root directory, on disk, to upload.
    :param daily_gdrive_id: The id of the Daily Statements folder on google drive.
    :param archive_gdrive_id: The id of the Archive folder on google drive.
    :param unmatch: Substring that the folder name must NOT contain
    :return:
    """
    for entry in os.scandir(root_directory):
        print("Possibly uploading " + str(entry.name))
        if unmatch is not None and unmatch in entry.name:
            print("Entry name contained unmatch string " + unmatch)
            continue
        if entry.is_dir() and match in entry.name:
            print("Uploading Entry")
            update_daily_savings_for_output_folder(entry.name, root_directory, daily_gdrive_id, archive_gdrive_id)


def move_existing_pg_folders(gdrive_directory: str, archive_directory: str, output_directory: str, suffix: str) -> None:
    """
    Moves folders that already exist in the pg_stem_folder location to pg_stem_archive, if they also exist in the
    output_directory location.

    :param gdrive_directory: The directory in Google Drive to search for matching folders in
    :param archive_directory: The destination archive directory
    :param output_directory: The output directory for the reprocessed PGs
    :param suffix: A pattern to match when moving folders, to avoid moving items that should not be moved
    """
    for _dir in os.scandir(output_directory):
        print("Deciding whether to move " + str(_dir.name))
        if _dir.is_file():
            continue
        folder_name = _dir.name
        if folder_name.endswith(suffix):
            print("Moving " + str(_dir.name))
            move_gdrive_file_with_name_and_parent(folder_name, gdrive_directory, archive_directory, False)


def process_existing_pg_statements(gdrive_directory: str, archive_directory: str, output_directory: str,
                                   suffix: str) -> None:
    """
    Moves folders that already exist in the pg_stem_folder location to pg_stem_archive, if they also exist in the
    output_directory location.

    :param gdrive_directory: The directory in Google Drive to search for matching folders in
    :param archive_directory: The destination archive directory
    :param output_directory: The output directory for the reprocessed PGs
    :param suffix: A pattern to match when moving folders, to avoid moving items that should not be moved
    """
    for _dir in os.scandir(output_directory):
        print("Deciding whether to process " + str(_dir.name))
        if _dir.is_file():
            continue
        folder_name = _dir.name
        if folder_name.endswith(suffix):
            print("Processing " + str(_dir.name))
            do_archive_and_remove_old_statements(folder_name, gdrive_directory, archive_directory)


def get_gdrive_file_id(starts_with: str, parent_id: str, api: gdrive_api.DriveAPI) -> str:
    """
    Gets the id of a "file" on Google Drive that starts with a given string. If there are multiple files with the
    given prefix, will give the id of the first one.

    :param starts_with: The prefix to search for.
    :param parent_id: The id of the containing folder on Google Drive
    :param api: A Google API object, so this function doesn't need to create one every time it's called
    :return: The id, or an empty string if no file is found.
    """
    results = api.get_file_names(starts_with=starts_with, parent_id=parent_id, folders_only=False)
    if len(results) > 0:
        print(f"Found {len(results)} drive files with prefix {starts_with}")
        return results.iloc[-1]['fileid']
    else:
        return ""


def update_daily_statement_or_upload_first(folder_name: str, ec2_directory: str, parent_id: str) -> None:
    """
    Update the daily statement folder with the new, regenerated information. If the folder doesn't exist,
    this function will create it and upload all available data.

    :param folder_name: The outer folder name for statements, eg S-000XXXX_SavStem.
    :param ec2_directory: The directory on the ec2 machine with the regenerated statements.
    :param parent_id: The ID of the folder on gdrive to operate in, usually one of the Daily Savings Statement folders
    """
    api = gdrive_api.DriveAPI()
    print(f"Finding ID for {folder_name}")
    statement_folder_id = get_gdrive_file_id(folder_name, parent_id, api)
    if len(statement_folder_id) <= 0:
        print(f"ID not found, creating folder for {folder_name}")
        statement_folder_id = api.create_folder(folder_name, parent_id)
    ec2_statement_folder = os.path.join(ec2_directory, folder_name)
    for _dir in os.scandir(ec2_statement_folder):
        if _dir.is_file():
            continue
        print(f"Trying to find folder id for {_dir.name}")
        statement_id = get_gdrive_file_id(_dir.name, statement_folder_id, api)
        if len(statement_id) > 0:
            print(f"Deleting Gdrive copy of folder id for {_dir.name}")
            api.delete_f(statement_id)
    print(f"Uploading folders in {ec2_statement_folder}")
    upload_subtree(ec2_statement_folder, statement_folder_id)


def get_gdrive_folder_or_create(folder_name: str, parent_id: str, api: gdrive_api.DriveAPI) -> str:
    """
    Either gets the id of an existing folder with folder_name, or makes it in the parent.

    :param folder_name: The folder name to look for
    :param parent_id: The parent folder to search
    :param api: A Google Drive API object
    :return: The id of the folder
    """
    return_id = get_gdrive_file_id(folder_name, parent_id, api)
    if len(return_id) <= 0:
        print(f"ID not found, creating folder for {folder_name} in parent with id {parent_id}.")
        return_id = api.create_folder(folder_name, parent_id)
    return return_id


def update_daily_savings_for_output_folder(folder_name: str, ec2_directory: str, daily_folder_id: str,
                                           archive_id: str) -> None:
    """
    Update the daily statement folder with the new, regenerated information. If the folder doesn't exist,
    this function will create it and upload all available data.

    :param folder_name: The outer folder name for statements, eg S-000XXXX_SavStem.
    :param ec2_directory: The directory on the ec2 machine with the regenerated statements.
    :param daily_folder_id: The gdrive directory of the Daily Savings Statement folders
    :param archive_id: The gdrive directory of the archive for old statements.
    """
    api = gdrive_api.DriveAPI()
    statement_folder_id = get_gdrive_folder_or_create(folder_name, daily_folder_id, api)
    statement_archive_folder_id = get_gdrive_folder_or_create(folder_name, archive_id, api)
    ec2_statement_folder = os.path.join(ec2_directory, folder_name)
    for _dir in os.scandir(ec2_statement_folder):
        if _dir.is_file():
            continue
        print(f"Trying to find folder id for {_dir.name} in daily statements.")
        statement_id = get_gdrive_file_id(_dir.name, statement_folder_id, api)
        if len(statement_id) > 0:
            print(f"Moving Gdrive copy of folder id for {_dir.name} to archive.")
            archive_or_erase_file(statement_id, statement_archive_folder_id, _dir.name, api)
    print(f"Uploading folders in {ec2_statement_folder}")
    upload_subtree(ec2_statement_folder, statement_folder_id)
