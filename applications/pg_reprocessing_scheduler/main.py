import argparse
import os
import shutil

import applications.pg_reprocessing_scheduler.shared as shared
import common.config_tools.manager as config_manager
import applications.pg_reprocessing_scheduler.definitions as local_definitions

# Get all systems with PGs
# Find out which PGs are ending soon
# Store that data OR maybe just rerun the PGs depending on what Tomi gives us
# Add this to the docker run script -v /home/stem$env_subdir/data/Output_PG:/stem-grovestreams/data/Output_PG
# _Sav goes in utility, _SavStem goes in stem

config = config_manager.get_module_config_from_root_path(local_definitions.root_path)

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--run_pgs", action="store_true",
                    help="Re-generate PG reports for each of the systems with PG end dates inside the target "
                         "window (Modifiable in config).")
parser.add_argument("-l", "--list_pgs", action="store_true",
                    help="List all PGs which would be reprocessed on a --run_pgs run without actually processing them.")
parser.add_argument("-s", "--sids", type=str, nargs='+',
                    help="Processes the oldest non-completed true-up term for a system or systems based on SID.")
parser.add_argument("-p", "--process_existing", action="store_true",
                    help=f"Process existing savings-ops statements present in the {config['output_directory_ec2']} "
                         f"folder without re-running savings-ops.")


def process_savings_ops_output():
    # upload the entire output directory to google drive
    shared.archive_existing_statements_and_replace("_SavStem", config['output_directory_ec2'], config['pg_stem_folder'],
                                                   config['pg_stem_archive'])
    shared.archive_existing_statements_and_replace("_Sav", config['output_directory_ec2'], config['pg_utility_folder'],
                                                   config['pg_utility_archive'], "_SavStem")
    shutil.rmtree(config['output_directory_ec2'])
    os.mkdir(config['output_directory_ec2'])


if __name__ == "__main__":
    args = parser.parse_args()
    if not (args.run_pgs or args.list_pgs or args.sids or args.process_existing):
        print("No mode selected.")
    if args.run_pgs:
        schedule = shared.create_schedule_df()
        print(schedule)
        for _, row in schedule.iterrows():
            start_end = shared.get_term_start_end(row['System_Rec_Id__c'])
            shared.rerun_sav_savstem_for_sid_2(row['System_Rec_Id__c'], config['output_directory_docker'],
                                               start_end['Term_Start__c'], start_end['Term_End__c'])
        process_savings_ops_output()
    if args.process_existing:
        process_savings_ops_output()
    if args.sids:
        for sid in args.sids:
            start_end = shared.get_term_start_end(sid)
            shared.rerun_sav_savstem_for_sid_2(sid, config['output_directory_docker'],
                                               start_end['Term_Start__c'], start_end['Term_End__c'])
        process_savings_ops_output()
    if args.list_pgs:
        schedule = shared.create_schedule_df()
        print(schedule)
