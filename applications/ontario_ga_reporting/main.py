import argparse
import datetime
import sys
import applications.ontario_ga_reporting.processors.trillium.trillium as trillium
import applications.ontario_ga_reporting.processors.logan_ieso.logan_ieso as logan_ieso


parser = argparse.ArgumentParser()
parser.add_argument("-t", "--trillium", type=str,
                    help="Output the Trillium report to the specified location. Will not run report is absent.")
parser.add_argument("-l", "--logan_ieso", type=str,
                    help="Output the Logan IESO report to the specified location. Will not run report is absent.")
parser.add_argument("-d", "--date", type=str,
                    help="The date, in format yyyy-mm, to use for report generation.")


def main():
    args = parser.parse_args()
    if not (args.trillium or args.logan_ieso):
        print("No output mode selected.")
        sys.exit(-1)
    if not args.date:
        print("No date selected.")
        sys.exit(-1)
    target_date = datetime.datetime.strptime(args.date, "%Y-%m")
    if args.trillium:
        trillium.create_report(target_date.year, target_date.month, args.trillium)
    if args.logan_ieso:
        logan_ieso.create_report(target_date.year, target_date.month, args.logan_ieso)


if __name__ == '__main__':
    main()
