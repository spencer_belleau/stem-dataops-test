import openpyxl
import pandas as pd
import datetime
import applications.ontario_ga_reporting.definitions as local_definitions
import common.config_tools.manager as config_manager
import common.grovestreams_api.dataframe_api as gst_api
import common.grovestreams_api.utils as gst_utils
import common.xlsx_reporter.worksheet_manipulator as ws_funcs
from typing import Callable

config = config_manager.get_module_config_from_root_path(local_definitions.root_path)


def __get_period_start_date(year: int, month: int) -> datetime.datetime:
    """
    Gets the first day of the given reporting period.

    :param year: The year of the beginning of the reporting period.
    :param month: The first month of the reporting period.
    :return: A datetime object the start of the billing period.
    """
    return datetime.datetime(year, month, 1, tzinfo=datetime.timezone.utc)


def __get_period_end_date(year: int, month: int) -> datetime.datetime:
    """
    Gets the last day of the given billing period, or the current date if that date is in the future.

    :param year: The year of the beginning of the reporting period.
    :param month: The first month of the reporting period.
    :return: A datetime object the end of the billing period.
    """
    eop = datetime.datetime(year+1, month, 1, tzinfo=datetime.timezone.utc) - datetime.timedelta(minutes=1)
    today = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    if eop > today:
        return today
    else:
        return eop


def __get_top_5_ga_events_full_hours(year: int, month: int) -> pd.DataFrame:
    """
    Gets the GA peak information in hourly format, sorted by time.
    Note: Still contains all hours which did NOT have a peak, peaks have data=1.0

    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return:
    """
    return get_top_5_ga_events_full(year, month).resample(datetime.timedelta(hours=1)).median().sort_values(by='time')


def find_template_row_range(sheet: openpyxl.workbook.workbook.Worksheet, col: int) -> tuple:
    """
    Finds the top and bottom of the working area by looking for #sheet#top# and #sheet#bottom# in the specified column.

    :param sheet: The worksheet to search.
    :param col: The column to search.
    :return: A tuple of (top, bottom). Both values are exclusive, and top is above the title line of the sheet.
    """
    top_index = find_tag_in_column(sheet, col, "#sheet#top#")
    bottom_index = find_tag_in_column(sheet, col, '#sheet#bottom#')
    return top_index, bottom_index


def find_tag_in_column(sheet: openpyxl.workbook.workbook.Worksheet, column: int, tag: str, max_blanks: int = 20) -> int:
    """
    Finds a given string tag in a column and returns the row number it was found on.
    Also clears that cell of data, since tags should not be in the final output.

    :param sheet: The worksheet to search
    :param column: The column to search in
    :param tag: The string tag to search for, ex: '#sheet#top#'
    :param max_blanks: The maximum amount of blank cells to tolerate before cancelling the find operation.
    :return: The integer number of the row which the tag was found on, or -1 if the tag was not found.
    """
    blanks = 0
    row = 1
    while blanks < max_blanks:
        cell_value = sheet.cell(row, column).value
        if str(cell_value) == tag:
            ws_funcs.set_cell_str(sheet, row, column, "")
            return row
        elif cell_value is None:
            blanks += 1
        else:
            blanks = 0
        row += 1
    return -1


def find_tag_in_row(sheet: openpyxl.workbook.workbook.Worksheet, row: int, tag: str, max_blanks: int = 50) -> int:
    """
    Finds a given string tag in a column and returns the column number it was found on.
    Also clears that cell of data, since tags should not be in the final output.

    :param sheet: The worksheet to search
    :param row: The row to search in
    :param tag: The string tag to search for, ex: '#sheet#top#'
    :param max_blanks: The maximum amount of blank cells to tolerate before cancelling the find operation.
    :return: The integer number of the column which the tag was found on, or -1 if the tag was not found.
    """
    blanks = 0
    column = 1
    while blanks < max_blanks:
        cell_value = sheet.cell(row, column).value
        if str(cell_value) == tag:
            ws_funcs.set_cell_str(sheet, row, column, "")
            return column
        elif cell_value is None:
            blanks += 1
        else:
            blanks = 0
        column += 1
    return -1


def get_column_indices_dict(sheet: openpyxl.workbook.workbook.Worksheet,
                            row: int, tags: list, max_blanks: int = 50) -> dict:
    """
    Takes a list of tags and finds the indices of each column in which they are found on a given row.

    :param sheet: The worksheet to search
    :param row: The row to search in
    :param tags: The string tags to search for, ex: '#sheet#top#', in a list
    :param max_blanks: The maximum amount of blank cells to tolerate before cancelling the find operation.
    :return: A dictionary with tag names as keys, and column indices as values
    """
    results = {}
    for tag in tags:
        results[tag] = find_tag_in_row(sheet, row, tag, max_blanks)
    return results


def get_year_ga_events_hours(year: int, month: int) -> pd.DataFrame:
    """
    Gets a dataframe with each hour on which a GA event took place in a given year.
    Note: This function converts a 5-minute interval stream to an hourly interval.

    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return: A dataframe with rows for each GA event, or an empty dataframe if GroveStreams failed to respond
    """
    print("Getting number of GA event hours.")
    df = gst_api.get_5min_data_between_dates("Ontario_GA_Events", "GA_Events_Full",
                                             __get_period_start_date(year, month), __get_period_end_date(year, month))
    df = df.fillna(0)
    df['time'] = df['time'].apply(gst_utils.gst_time_to_datetime)
    df['data'] = df['data'].astype(float)
    df = df.resample(datetime.timedelta(hours=1), on='time').median().sort_values('time')
    df = df[df['data'] == 1.0]
    return df


def get_top_5_ga_events_full(year: int, month: int) -> pd.DataFrame:
    """
    Gets the data for a given year from the Top5_GA_Events_Full stream.
    Does not remove False rows from the data.

    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return: A dataframe with rows for each GA event, or an empty dataframe if GroveStreams failed to respond
    """
    df = gst_api.get_5min_data_between_dates("Ontario_GA_Events", "ae863e32-2c59-3e9f-b563-1a6548717834",
                                             __get_period_start_date(year, month), __get_period_end_date(year, month))
    df['data'] = df['data'].fillna(0)
    df['time'] = df['time'].apply(gst_utils.gst_time_to_datetime)
    return df.set_index('time')


def get_year_ga_events_days(year: int, month: int) -> pd.DataFrame:
    """
    Gets a dataframe with each day on which a GA event took place in a given year.

    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return: A dataframe with rows for each GA event, or an empty dataframe if GroveStreams failed to respond
    """
    print("Getting number of GA event days.")
    df = gst_api.get_daily_data_between_dates("Ontario_GA_Events", "GA_Events_Full_Days",
                                              __get_period_start_date(year, month), __get_period_end_date(year, month))
    df = df.dropna()
    df = df[df.data]
    df['time'] = df['time'].apply(gst_utils.gst_time_to_datetime)
    return df.set_index('time')


def get_hourly_ontario_demand(year: int, month: int) -> pd.DataFrame:
    """
    Gets a dataframe with the hourly Ontario Demand Rates for given year.

    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return: A dataframe with rows for hour's rate, or an empty dataframe if GroveStreams failed to respond
    """
    df = gst_api.get_hourly_data_between_dates("Demand", "Ontario_Demand",
                                               __get_period_start_date(year, month), __get_period_end_date(year, month))
    df['data'] = df['data'].fillna(0)
    df['time'] = df['time'].apply(gst_utils.gst_time_to_datetime)
    return df.set_index('time')


def get_site_inverter_rating(site_id: str) -> float:
    """
    Gets the inverter rating in kW for a given site.
    It's a property, but it needs to be pulled down like a stream.
    Why? Ask GroveStreams.

    :param site_id: The site ID to measure
    :return: A float representing the kW inverter rating
    """
    df = gst_api.get_component_stream(site_id + "Stem", "SystemSize_kW",
                                      datetime.datetime.now() - datetime.timedelta(hours=1), datetime.datetime.now())
    if df is None:
        raise Exception("Failed to get site inverter rating, GroveStreams may be down.")
    return float(df.iloc[0]['data'])


def get_site_hourly_kw_discharge(site_id: str, year: int, month: int) -> pd.DataFrame:
    """
    Gets the hourly discharge rate in kW for a given site.
    Note: This function converts a 15-minute interval stream to an hourly interval.

    :param site_id: The site ID to measure
    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return: A float representing the hourly kW discharge
    """
    start = __get_period_start_date(year, month)
    end = __get_period_end_date(year, month)
    df = gst_api.get_15min_data_between_dates(site_id + "Stem", "ChgDischgkW", start - datetime.timedelta(seconds=-1),
                                              end)
    df = df.fillna(0)
    df['time'] = df['time'].apply(gst_utils.gst_time_to_datetime)
    full_intervals_df = pd.date_range(start=start, end=end, freq='15T').to_frame()
    df = full_intervals_df.join(df.set_index('time'), how="left")
    df = df.fillna(0).reset_index()[['index', 'data']]
    df = df.rename(columns={'index': 'time', 'data': 'data'})
    df = df.resample(datetime.timedelta(hours=1), on='time').mean().sort_values('time')
    return df


def get_estimated_peak_sizes(year: int, month: int) -> pd.DataFrame:
    """
    Gets the sizes of the five estimated GA peaks in a given period.

    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return: A dataframe containing the five peak sizes in MW
    """
    print("Getting estimated GA peak sizes.")
    top5_ga_events = __get_top_5_ga_events_full_hours(year, month)
    top5_ga_events = top5_ga_events[top5_ga_events['data'] != 0]
    ontario_demand = get_hourly_ontario_demand(year, month).sort_values(by='time')
    estimated_ga_peak_size = (top5_ga_events * ontario_demand).dropna()
    return estimated_ga_peak_size


def get_estimated_peak_performance(site_id: str, year: int, month: int) -> pd.DataFrame:
    """
    Gets a site's performance during the five GA peaks in a given period.

    :param site_id: The site ID whose performance is being measured.
    :param year: The billing period's starting year.
    :param month: The billing period's starting month.
    :return: A dataframe containing the five peak performance metrics
    """
    print("Getting estimated GA peak performance for site {site}.".format(site=site_id))
    top5_ga_events = __get_top_5_ga_events_full_hours(year, month)
    top5_ga_events = top5_ga_events[top5_ga_events['data'] != 0]
    hourly_discharge = get_site_hourly_kw_discharge(site_id, year, month)
    inverter_rating = get_site_inverter_rating(site_id)
    estimated_peak_performance = (hourly_discharge * top5_ga_events / inverter_rating * -1).dropna()
    return estimated_peak_performance


def set_metrics_by_indices_list(sheet: openpyxl.workbook.workbook.Worksheet, row: int, metric_indices: list,
                                metrics: list, formatter: Callable) -> None:
    """
    Sets a number of cells in a given row to the floating point values of the metrics provided.
    Note: This function assumes that there are an equal amount of metric indices and metrics, and that
    the order of the metric indices matches the order of the metrics

    :param sheet: The worksheet containing the cells
    :param row: The row to set the cells on
    :param metric_indices: A list of column ids
    :param metrics: A list of values which can be converted to floats (or already are floats) to place into the cells
    :param formatter: A function to be used in printing the metrics. Must output a string based on the metric value.
    """
    for i in range(0, len(metric_indices)):
        ws_funcs.set_cell_str(sheet, row, metric_indices[i], formatter(metrics[i]))


def get_metric_indices(all_metrics: dict, match: str) -> list:
    """
    Finds all column indices with a given string match.
    Ex: Given a dictionary with structure {"key1":1, "key2":2, "key3":3, "key20":4}, providing a match string
    of "key2" would return a list containing [2, 4]
    Technically this function does not require the values of the dictionary to be integers, but it is
    intended for use with get_column_indices_dict().

    :param all_metrics: A dictionary with string keys.
    :param match: A substring or string to use to filter the dictionary's keys.
    :return: A list of the values for keys which matched the provided match substring.
    """
    indices = []
    for key in all_metrics.keys():
        if match in key:
            indices.append(all_metrics[key])
    return indices


def set_dispatches(sheet: openpyxl.workbook.workbook.Worksheet, row: int, column_indices: dict, metric: int) -> None:
    """
    Sets the "YTD GA Dispatches" cell for a given row to the specified value.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the cell on
    :param column_indices: The full column indices dictionary for the sheet
    :param metric: The value to set the cell to
    """
    ws_funcs.set_cell_int(sheet, row, column_indices['#ytd#dispatches#'], metric)


def set_average_discharge_duration(sheet: openpyxl.workbook.workbook.Worksheet, row: int, column_indices: dict,
                                   metric: float) -> None:
    """
    Sets the "YTD Average Duration of GA Discharge" cell for a given row to the specified value.

    :param sheet: The worksheet containing the cell
    :param row: The row to set the cell on
    :param column_indices: The full column indices dictionary for the sheet
    :param metric: The value to set the cell to
    """
    ws_funcs.set_cell_float(sheet, row, column_indices['#ytd#average#discharge#'], metric)


def set_estimated_ga_peak_size_metrics(sheet: openpyxl.workbook.workbook.Worksheet, row: int, column_indices: dict,
                                       metrics: list) -> None:
    """
    Sets all "YTD Estimated GA Peak Size (MW)" cells for a given row to the values specified.
    Note: This function assumes that there are an equal amount of "YTD Estimated GA Peak Size (MW)" cells and
    metrics in the list provided. Ex: If there are "YTD Estimated GA Peak Size (MW) - Peak 1" through
    "YTD Estimated GA Peak Size (MW) - Peak 5", there should be five metrics in the list.

    :param sheet: The worksheet containing the cells
    :param row: The row to set the cells on
    :param column_indices: The full column indices dictionary for the sheet
    :param metrics: A list of the metrics to populate the cells with. These will be interpreted as float values.
    """
    estimated_peak_size_cols = get_metric_indices(column_indices, '#ytd#estimated#peak#')
    set_metrics_by_indices_list(sheet, row, estimated_peak_size_cols, metrics, ws_funcs.format_two_decimal_places)


def set_estimated_performance_metrics(sheet: openpyxl.workbook.workbook.Worksheet, row: int, column_indices: dict,
                                      metrics: list) -> None:
    """
    Sets all "YTD Estimated GA Performance" cells for a given row to the values specified.
    Note: This function assumes that there are an equal amount of "YTD Estimated GA Peak Size (MW)" cells and
    metrics in the list provided. Ex: If there are "YTD Estimated GA Performance - Peak 1" through
    "YTD Estimated GA Performance - Peak 5", there should be five metrics in the list.

    :param sheet: The worksheet containing the cells
    :param row: The row to set the cells on
    :param column_indices: The full column indices dictionary for the sheet
    :param metrics: A list of the metrics to populate the cells with. These will be interpreted as float values.
    """
    estimated_peak_performance_cols = get_metric_indices(column_indices, '#ytd#estimated#performance#')
    set_metrics_by_indices_list(sheet, row, estimated_peak_performance_cols, metrics, ws_funcs.format_percent_100)


def set_estimated_savings_metrics(sheet: openpyxl.workbook.workbook.Worksheet, row: int, column_indices: dict,
                                  metrics: list) -> None:
    """
    Sets all "YTD Estimated GA Savings" cells for a given row to the values specified.
    Note: This function assumes that there are an equal amount of "YTD Estimated GA Peak Size (MW)" cells and
    metrics in the list provided. Ex: If there are "YTD Estimated GA Savings - Peak 1" through
    "YTD Estimated GA Savings - Peak 5", there should be five metrics in the list.

    :param sheet: The worksheet containing the cells
    :param row: The row to set the cells on
    :param column_indices: The full column indices dictionary for the sheet
    :param metrics: A list of the metrics to populate the cells with. These will be interpreted as float values.
    """
    estimated_ga_savings_cols = get_metric_indices(column_indices, '#ytd#estimated#savings#')
    set_metrics_by_indices_list(sheet, row, estimated_ga_savings_cols, metrics, ws_funcs.format_currency)


def set_period_cell_string(sheet: openpyxl.workbook.workbook.Worksheet, year: int, month: int) -> None:
    """
    Sets the A6 cell of the specified worksheet to have an appropriate reporting period.

    :param sheet: The worksheet to process.
    :param year: The reporting period year as given to the script.
    :param month: The reporting period month as given to the script.
    """
    start = __get_period_start_date(year, month)
    end = __get_period_end_date(year, month)
    sheet['A6'].value = f"Period: {start.strftime('%Y-%m-%d %H:%M')} to {end.strftime('%Y-%m-%d %H:%M')}"


def process_template(sheet: openpyxl.workbook.workbook.Worksheet, year: int, month: int) -> None:
    """
    Processes a template xlsx sheet, fetches the required metrics, and fills out the information.

    :param sheet: The sheet to process
    :param year: The year to use when processing.
    :param month: The month to use when processing.
    """
    top, bottom = find_template_row_range(sheet, 1)
    column_indices = get_column_indices_dict(sheet, top, config["Input Rows"])
    set_period_cell_string(sheet, year, month)
    ga_events_days = get_year_ga_events_days(year, month)
    ga_events_hours = get_year_ga_events_hours(year, month)
    estimated_ga_peak_size = get_estimated_peak_sizes(year, month)
    for row in range(top+2, bottom):
        site_id = sheet.cell(row, 1).value
        estimated_peak_performance = get_estimated_peak_performance(site_id, year, month)
        estimated_ga_value = sheet.cell(row, column_indices['#estimated#ga#value#']).value
        estimated_ga_savings = estimated_peak_performance * estimated_ga_value * 1000
        set_dispatches(sheet, row, column_indices, len(ga_events_days))
        set_average_discharge_duration(sheet, row, column_indices,
                                       len(ga_events_hours) / float(len(ga_events_days)))
        set_estimated_ga_peak_size_metrics(sheet, row, column_indices, estimated_ga_peak_size['data'].tolist())
        set_estimated_performance_metrics(sheet, row, column_indices,
                                          estimated_peak_performance['data'].tolist())
        set_estimated_savings_metrics(sheet, row, column_indices, estimated_ga_savings['data'].tolist())
