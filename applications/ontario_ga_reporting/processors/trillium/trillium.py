import openpyxl
import common.config_tools.manager as config_manager
import applications.ontario_ga_reporting.definitions as local_definitions
import applications.ontario_ga_reporting.processors.shared as shared
import os


config = config_manager.get_module_config_from_root_path(local_definitions.root_path)
local_path = os.path.dirname(os.path.abspath(__file__))


def create_report(year: int, month: int, output_report_name: str):
    wb_path = os.path.join(local_path, 'Trillium Reporting Template.xlsx')
    template_workbook = openpyxl.load_workbook(wb_path)
    template_worksheet = template_workbook.worksheets[0]
    shared.process_template(template_worksheet, year, month)
    template_workbook.save(output_report_name)
