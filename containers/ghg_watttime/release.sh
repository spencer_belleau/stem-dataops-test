container_name="ghg_watttime"
version="v1.0"

cd ../../

docker stop "$container_name"
docker rm "$container_name"
docker rmi "$container_name":"$version"
docker build --tag "$container_name":"$version" --file containers/ghg_watttime/Dockerfile .
docker run -t -d --name "$container_name" "$container_name":"$version" 
docker start "$container_name"

docker image ls
docker ps -a
