import os
import sys
import argparse
import site

local_path = os.path.dirname(os.path.abspath(__file__))
site_path = site.USER_SITE
path_filename = os.path.join(site_path, 'stem-dataops.pth')

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--uninstall", action="store_true",
                    help="Uninstall instead of installing.")


def main() -> None:
    args = parser.parse_args()
    print(site_path)
    if not args.uninstall:
        if not os.path.exists(site_path):
            os.makedirs(site_path)
        with open(path_filename, 'w') as file:
            file.writelines([local_path])
    else:
        if os.path.exists(site_path):
            os.remove(path_filename)


if __name__ == '__main__':
    main()
